<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?php echo $pageTitle; ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets/plugins/font-awesome-4.3.0/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets/plugins/ionicons-2.0.1/css/ionicons.min.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets/plugins/datepicker/datepicker3.css');?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets/plugins/datatables/dataTables.responsive.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets/dist/css/AdminLTE.min.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets/dist/css/skins/_all-skins.min.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets/plugins/select2/select2.min.css') ?>" rel="stylesheet" type="text/css" />
	<script src="<?= base_url('assets/plugins/jQuery/jquery-2.2.3.min.js') ?>"></script>
	<script src="<?= base_url('assets/plugins/fastclick/fastclick.min.js') ?>" type="text/javascript"></script>
	<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.js') ?>" type="text/javascript"></script>
	<script src="<?= base_url('assets/plugins/datepicker/bootstrap-datepicker.js') ?>" type="text/javascript"></script>
	<script src="<?= base_url('assets/plugins/datatables/dataTables.tableTools.js') ?>" type="text/javascript"></script>
	<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap.js') ?>" type="text/javascript"></script>
	<script src="<?= base_url('assets/plugins/datatables/dataTables.responsive.min.js') ?>" type="text/javascript"></script>
	<script src="<?= base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('assets/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= base_url('assets/dist/js/app.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= base_url('assets/js/jquery.validate.js'); ?>" type="text/javascript"></script>
    <script src="<?= base_url('assets/js/validation.js'); ?>" type="text/javascript"></script>	
	<script src="<?= base_url('assets/plugins/select2/select2.full.min.js') ?>"></script>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="../../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="../../https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
    <style>
    	.error{
    		color:red;
    		font-weight: normal;
    	}
    </style>
    <script type="text/javascript">
        var baseURL = "<?php echo base_url(); ?>";
    </script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
		<header class="main-header">
        <a href="<?php echo base_url(); ?>" class="logo">
          <span class="logo-lg"><b>SIP</b>us YP GAMA</span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?php echo $username; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="user-header">
                    <img src="<?php echo base_url('assets/dist/img/avatar.png'); ?>" class="img-circle" alt="User Image" />
                    <p>
                      <?php echo $username; ?>
                      <small><?php echo $no_identitas; ?></small>
                    </p>
                  </li>
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url('loadChangePass'); ?>" class="btn btn-default btn-flat"><i class="fa fa-key"></i> Ganti Password</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url('logout'); ?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Keluar</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <aside class="main-sidebar">
        <section class="sidebar">
          <ul class="sidebar-menu">
            <li class="header">Navigasi Utama</li>
            <?php if($usertype == ROLE_ADMIN){ ?>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>
			
			<li class="treeview">
                <a href="#"><i class="fa fa-list-alt"></i> <span>Data Pengguna</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu" style="display: none;">		
					<li>
						<a href="<?php echo base_url('user').'/siswa';?>"><i class="fa fa-list-alt"></i>
							<span>Anggota Siswa</span>
						</a>
					</li>
					<li>
					  <a href="<?php echo base_url('kelas'); ?>" ><i class="fa fa-files-o"></i>
						<span>Kelas</span>
					  </a>
					</li>
					<li>
						<a href="<?php echo base_url('user').'/guru_staff'; ?>"><i class="fa fa-files-o"></i>
							<span>Anggota Guru/Staff</span>
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('user').'/petugas'; ?>"><i class="fa fa-list-alt"></i>
							<span>Petugas Perpustakaan</span>
						</a>
					</li>
				</ul>
            </li>
			<li class="treeview">
                <a href="#"><i class="fa fa-list-alt"></i> <span>Buku</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu" style="display: none;">
					<li>
						<a href="<?= base_url('buku') ?>"><i class="fa fa-list-alt"></i>
						<span>Data Buku</span>
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('buku/kategoribuku'); ?>"><i class="fa fa-list-alt"></i>
							<span>Kategori Buku</span>
						</a>
					</li>					
					<li>
						<a href="#"><i class="fa fa-list-alt"></i> <span>Peminjaman Buku</span><i class="fa fa-angle-left pull-right"></i></a>
						<ul class="treeview-menu" style="display: none;">
							<li>
								<a href="<?php echo base_url('buku/peminjamansiswa'); ?>"><i class="fa fa-list-alt"></i>
									<span>Siswa</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('buku/peminjamangurustaff'); ?>"><i class="fa fa-list-alt"></i>
									<span>Guru / Staff</span>
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="<?php echo base_url('buku/historipengembalian'); ?>"><i class="fa fa-list-alt"></i> <span>Pengembalian Buku</span></a>
					</li>
					<li>
						<a href="<?php echo base_url('buku/kehilanganbuku'); ?>"><i class="fa fa-list-alt"></i> <span>Kehilangan Buku</span></a>
					</li>
				</ul>
            </li>
			<li>
				<a href="<?php echo base_url('laporan'); ?>"><i class="fa fa-ticket"></i>
					<span>Laporan</span>
				</a>
			</li>
            <?php
            }
            ?>
			 <?php if(($usertype == ROLE_GURU_STAFF) || ($usertype == ROLE_MURID)){ ?>
			<li class="treeview">
              <a href="<?php echo base_url(); ?>dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>
			<li class="treeview">
				<a href="<?php echo base_url('buku'); ?>"><i class="fa fa-list-alt"></i>
					<span>Daftar Buku</span>
				</a>
			</li>
			 <?php } ?>
          </ul>
        </section>
      </aside>