<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content">
            <div class="row">
              <div class="col-xs-12">
			  <div class='box'>
                <div class='box-header'>
					<ol class="breadcrumb">
						<li><a href="<?=base_url()?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
						<li class="active"><?= $keterangan ?></li>
					</ol>
				</div>
				<form action="<?php echo base_url('buku/tambahpengembalianbuku') ?>" method="post" role="form" class="from-horizontal" id="tambahpengembalianbuku">
                    <div class="box-body">
						<div class="row"> 
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-12"><h3>Detail Informasi Buku </h3><hr></label>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">No. Induk Buku<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->NIB ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Kategori<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->nama_kategori ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Jenis Buku<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->jenis_buku ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Penerbit<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->penerbit ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Pengarang<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->pengarang ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">ISBN<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->ISBN ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Edisi<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->edisi ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Bahasa<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->bahasa ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Asal<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->asal ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Tanggal Masuk<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->tgl_masuk ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Tahun Terbit<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->tahun_terbit ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Stok Awal<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->jumlah ?><hr>
									</div>
                                </div>
                            </div>
							<!--// Awal Info Tambahan Mengenai Buku   -->
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Siswa<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										Meminjam <?= $pinjamsiswa=$jumlahpeminjamansiswa->total_buku-0 ?> Buku<hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Guru/Staff<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										Meminjam <?= $pinjamguru=$jumlahpeminjamangurustaff->total_buku-0 ?> Buku<hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Jumlah Hilang<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $pinjampetugas=$jumlahpeminjamanpetugas->jumlah_hilang-0 ?> Buku<hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Stock Akhir<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $stokakhir=($detailbuku->jumlah)-$pinjamguru-$pinjampetugas-$pinjamsiswa ?><hr>
									</div>
                                </div>
                            </div>
							<!--// Akhir Info Tambahan Mengenai Buku   -->
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Deskirpsi Buku<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->deskripsi_buku ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Lokasi Buku<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->lokasi_buku ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-12"><hr><h3>Detail Informasi Peminjaman</h3><hr></label>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-4">Nama Peminjam<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-7">
										<?= $peminjaman->nama ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-4">Predikat<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-7">
										<?= $predikat ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-4">Dipinjam<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-7">
										<?= $peminjaman->total_buku ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-4">Status Peminjaman<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-7">
										<?php if ($peminjaman->sudah_kembali_atau_belum=="t"){
											echo "Belum Dikembalikan";
										} else { echo "Sudah Dikembalikan";}?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-4">Tanggal Peminjaman<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-7">
										<?= $peminjaman->tgl_pinjam ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-4">Batas Pengembalian<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-7">
										<?= $peminjaman->tgl_harus_kembali ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-12"><hr><h3>Masukkan Data Pengembalian</h3><hr></label>
                                </div>
                            </div>
                        </div>
						<div class="row">
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-md-4">Kembalikan :<hr></label>
									<div class="col-md-8">
										<select class="form-control select2" name="total" id="total">
											<option value="Semua">Semua Buku</option>
											<?php
											$n=($peminjaman->total_buku)-($jumlahdikembalikan->total_buku_dikembalikan)-1;
											for($i = $n; $i > 0 ; $i--)
											{ ?><option value="<?= $i; ?>"><?= $i; ?> Buku</option>
											<?php } ?>
										</select><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-md-4">Tanggal Pengembalian :<hr></label>
									<div class="col-md-8">
										<input type="text" class="form-control required date" id="tgl_buku_dikembalikan" name="tgl_buku_dikembalikan" maxlength="14" value="<?= date('Y-m-d'); ?>"><hr>
									</div>
                                </div>
                            </div>
						</div>
						<div class="row">
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-md-4">Keterangan :<hr></label>
									<div class="col-md-8">
										<?php
										$awal = date('Ymd', strtotime($peminjaman->tgl_harus_kembali));
										$akhir = date('Ymd'); 
										if (($awal>$akhir) || ($awal==$akhir)){
											echo "( Batas Pengembalian ".$peminjaman->tgl_harus_kembali." )";
											$keteranganketerlambatan=0;
										} else {
										if ($keteranganketerlambatan<0){
											echo "( Batas Pengembalian ".$peminjaman->tgl_harus_kembali." )";
											$keteranganketerlambatan=0;
										} else {
											echo "Terlambat ".$keteranganketerlambatan." Hari";
										}} ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-md-4">Denda Terlambat :<hr></label>
									<div class="col-md-8">
										<?php echo $keteranganketerlambatan*1000 ?>
										<input type="hidden" class="" id="denda" name="denda" value="<?php echo $keteranganketerlambatan*1000 ?>" onchange="totaldenda()"><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-md-4">Denda Kerusakan Buku :<hr></label>
									<div class="col-md-8">
										<input type="number" class="form-control required" id="denda_kerusakan_buku" name="denda_kerusakan_buku" maxlength="14" value="0" onkeyup="totaldenda()"><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-md-4">Denda Kehilangan Buku :<hr></label>
									<div class="col-md-8">
										<input type="number" class="form-control required" id="denda_kehilangan_buku" name="denda_kehilangan_buku" maxlength="14" value="0" onkeyup="totaldenda()"><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-md-4">Total Denda Buku :<hr></label>
									<div class="col-md-8">
										<div id="total_denda"><?php echo $keteranganketerlambatan*1000 ?></div><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-md-4">Jumlah Denda Dibayarkan :<hr></label>
									<div class="col-md-8">
										<input type="number" class="form-control required" id="jumlah_denda_dibayarkan" name="jumlah_denda_dibayarkan" maxlength="6" value="0"><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-md-4">Keterangan Tambahan :<hr></label>
									<div class="col-md-8">
										<input type="text" class="form-control required" id="keterangan" name="keterangan" maxlength="140" placeholder="Keterangan Tambahan" value=""><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-12"><hr></label>
                                </div>
                            </div>
						</div>
                    </div>
                    <div class="box-footer">
						<input name="id_buku" value="<?= $detailbuku->NIB ?>" type="hidden">
						<input name="id" value="<?= $peminjaman->id_peminjaman ?>" type="hidden">
                        <input type="submit" class="btn btn-primary" value="Kembalikan Buku" />
                        <input type="reset" class="btn btn-default" value="Reset" />
                    </div>
                </form>
			  </div>
			<script src="<?php echo base_url(); ?>assets/js/tambahpengembalianbuku.js" type="text/javascript"></script>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">List Transaksi ID Peminjaman <?= $peminjaman->id_peminjaman ?></a></li>
                  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                        <table id="mytable" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                          <thead>
                            <tr>
							  <th class="col-xs-1">ID Pengembalian</th>
							  <th class="col-xs-2">No Induk Buku</th>
							  <th class="col-xs-1">Jumlah Dikembalikan</th>
							  <th class="col-xs-2">Denda</th>
							  <th class="col-xs-1">Tanggal Dikembalikan</th>
                            </tr>
                          </thead>
                          <tbody>	
							<?php
							if(!empty($pengembalian))
							{
								foreach($pengembalian as $record)
								{
							?>
							<tr>
							  <td><?php echo $record->id_pengembalian ?></td>
							  <td><?php echo $record->NIB ?></td>
							  <td><?php echo $record->total_buku_dikembalikan ?></td>
							  <td><?php echo $record->denda ?></td>
							  <td><?php echo $record->tgl_buku_dikembalikan ?></td>
							</tr>
							<?php
								}
							}
							?>
						  </tbody>
                        </table>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div>                
              </div><!-- /.col -->
            </div><!-- /.row -->
          </section><!-- /.content -->
        <script type="text/javascript">
		$(document).ready(function() { var table = $('#mytable').DataTable(); } );
			
		$(function(){$(".select2").select2();});
		$(function () {
		$('.date').datepicker({
		  format: 'yyyy-mm-dd',
		  autoclose: true,
			});
		  });
		function totaldenda() {
			var denda = parseInt(document.getElementById("denda").value);
			var denda_kerusakan_buku = parseInt(document.getElementById("denda_kerusakan_buku").value);
			var denda_kehilangan_buku = parseInt(document.getElementById("denda_kehilangan_buku").value);
			var total=denda+denda_kerusakan_buku+denda_kehilangan_buku;
			$('#total_denda').text(total);
		}
		
        </script>
</div>