<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content">
            <div class="row">
              <div class="col-xs-12">
			  <div class='box'>
                <div class='box-header'>
					<ol class="breadcrumb">
						<li><a href="<?=base_url()?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
						<li class="active"><?= $keterangan ?></li>
					</ol>
				</div>
			  </div>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?= $keterangan ?></a></li>
                  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                        <table id="mytable" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                          <thead>
                            <tr>
							  <th class="col-xs-1">Id Pinjam</th>
							  <th class="col-xs-2">Judul Buku</th>
							  <th class="col-xs-1">Kembali</th>
							  <th class="col-xs-1">Denda</th>
							  <th class="col-xs-1">Sisa Denda</th>
							  <th class="col-xs-1">Tanggal Kembali</th>
							  <th class="col-xs-3">Keterangan</th>
							  <th class="col-xs-1">Aksi</th>
                            </tr>
                          </thead>
                          <tbody>	
							<?php
							if(!empty($pengembalian))
							{
								foreach($pengembalian as $record)
								{
							?>
							<tr>
							  <td><?php echo $record->id_peminjaman ?></td>
							  <td><?php echo $record->judul_buku ?></td>
							  <td><?php echo $record->total_buku_dikembalikan ?></td>
							  <td><?php echo $jumlahdenda=$record->denda_keterlambatan+$record->denda_kerusakan_buku+$record->denda_kehilangan_buku ?></td>
							  <td><?php echo $record->jumlah_denda_dibayarkan-$jumlahdenda ?></td>
							  <td><?php echo $record->tgl_buku_dikembalikan ?></td>
							  <td><?php echo $record->keterangan ?></td>
							  <td class="text-center">
								  <a class="btn btn-sm btn-warning" title="Perbarui Data Pengembalian Buku" href="<?php echo base_url().'buku/EditPengembalianBuku/'.$record->id_pengembalian; ?>"><i class="fa fa-pencil-square-o"></i></a>
								  <button type="button" name="delete" id="<?php echo $record->id_pengembalian; ?>" class="btn btn-sm btn-danger delete" title="Hapus Pengembalian Buku"><i class="fa fa-trash"></i></button>
							  </td>
							</tr>
							<?php
								}
							}
							?>
						  </tbody>
                        </table>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div>                
              </div><!-- /.col -->
            </div><!-- /.row -->
          </section><!-- /.content -->
        <script type="text/javascript">
			$(document).ready(function() { var table = $('#mytable').DataTable(); } );
			
			$(document).on('click', '.delete', function(){  
			   var id_pengembalian = $(this).attr("id");  
			   if(confirm("Apakah Anda Yakin Ingin Menghapus Pengembalian Ini?"))  
			   {  
					$.ajax({  
						 url:"<?php echo base_url('buku/deletepengembalianbuku'); ?>",  
						 method:"POST",  
						 data:{id_pengembalian:id_pengembalian}  
					});
					var row = $(this).closest("tr");
					$('#mytable').DataTable().row(row).remove().draw();
			   }  
			   else  
			   {  
					return false;       
			   }  
			}); 
        </script>
</div>
