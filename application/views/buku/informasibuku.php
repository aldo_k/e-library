<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content">
            <div class="row">
              <div class="col-xs-12">
			  <div class='box'>
                <div class='box-header'>
					<ol class="breadcrumb">
						<li><a href="<?=base_url()?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
						<li><a href="<?= base_url('buku') ?>"/>List Buku</a></li>
						<li class="active"><?= $keterangan ?></li>
					</ol>
				</div>
				<form action="#" method="post" role="form" class="from-horizontal" id="">
                    <div class="box-body">
						<div class="row"> 
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-12">
										<h3>Detail Informasi Buku</h3>
										<hr>
									</label>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">No. Induk Buku<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->NIB ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Kategori<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->nama_kategori ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Jenis Buku<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->jenis_buku ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Penerbit<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->penerbit ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Pengarang<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->pengarang ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">ISBN<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->ISBN ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Edisi<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->edisi ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Bahasa<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->bahasa ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Asal<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->asal ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Tanggal Masuk<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->tgl_masuk ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Tahun Terbit<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->tahun_terbit ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Stok Awal<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->jumlah ?><hr>
									</div>
                                </div>
                            </div>
							<!--// Awal Info Tambahan Mengenai Buku   -->
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Siswa<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										Meminjam <?= $pinjamsiswa=$jumlahpeminjamansiswa->total_buku-0 ?> Buku<hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Guru/Staff<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										Meminjam <?= $pinjamguru=$jumlahpeminjamangurustaff->total_buku-0 ?> Buku<hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Jumlah Hilang<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $pinjampetugas=$jumlahpeminjamanpetugas->jumlah_hilang-0 ?> Buku<hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Stock Akhir<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $stokakhir=($detailbuku->jumlah)-$pinjamguru-$pinjampetugas-$pinjamsiswa ?><hr>
									</div>
                                </div>
                            </div>
							<!--// Akhir Info Tambahan Mengenai Buku   -->
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Deskirpsi Buku<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->deskripsi_buku ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Lokasi Buku<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->lokasi_buku ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-12"><hr></label>
                                </div>
                            </div>
                        </div>
					</div>
                    <div class="box-footer">
						<a href="<?= base_url('buku') ?>" class="btn btn-lg btn-danger"/>Kembali</a>
                    </div>
                </form>
			  </div>
			<script src="<?php echo base_url(); ?>assets/js/tambahpinjambuku.js" type="text/javascript"></script>
             </div><!-- /.col -->
            </div><!-- /.row -->
          </section><!-- /.content -->
        <script type="text/javascript">
			$(document).ready(function() { var table = $('#mytable').DataTable(); } );
			
			$(function(){$(".select2").select2();});
			$(function () {
			$('.date').datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
				});
			});
        </script>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_formPeminjam" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">NIS</label>
                            <div class="col-md-9 no_identitas">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Lengkap</label>
                            <div class="col-md-9 nama">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tempat Lahir</label>
                            <div class="col-md-9 tempat_lahir">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal Lahir</label>
                            <div class="col-md-9 tgl_lahir">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Telepon</label>
                            <div class="col-md-9 telepon">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Jenis Kelamin</label>
                            <div class="col-md-9 jenis_kelamin">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Jurusan</label>
                            <div class="col-md-9 jurusan">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Kelas</label>
                            <div class="col-md-9 kelas">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Alamat</label>
                            <div class="col-md-9 alamat">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Username</label>
                            <div class="col-md-9 username">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Foto</label>
                            <div class="col-md-9 foto">
                                
                            </div>
                        </div>
						
                    </div>
					</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

<!-- Bootstrap modal -->
<div class="modal fade" id="sudahkembali" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Buku Yg Dipinjam Sudah Dikembalikan</h3>
            </div>
            <div class="modal-body form">
                Peminjam Ini Sudah Mengembalikan Buku Yg Dipinjamnya...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Oke</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->