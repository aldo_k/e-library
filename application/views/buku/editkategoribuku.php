<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content">
            <div class="row">
              <div class="col-md-6">
			  <div class='box'>
                <div class='box-header'>
					<ol class="breadcrumb">
						<li><a href="<?=base_url()?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
						<li class="active"><?= $keterangan ?></li>
					</ol>
				</div>
					<form role="form" id="tambahKategoribuku" action="<?php echo base_url('buku/editkategoribukunya') ?>" method="post" role="form">
                        <div class="box-body">
							<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="kategori">Kategori Buku</label>
										<input type="text" class="form-control required" id="kategori" name="kategori" maxlength="32" value="<?= $nama_kategori ?>">
										<input type="hidden" class="" id="id_kategori" name="id_kategori" value="<?= $id_kategori ?>">
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Tambah <?= $keterangan ?>" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
					<script src="<?php echo base_url(); ?>assets/js/tambahKategoribuku.js" type="text/javascript"></script>
			  </div>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?= $keterangan ?></a></li>
                  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                        <table id="mytable" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                          <thead>
                            <tr>
							  <th class="col-xs-1">No</th>
							  <th class="col-xs-5">Kategori Buku</th>
							  <th class="col-xs-3">Aksi</th>
                            </tr>
                          </thead>
                          <tbody>	
							<?php
							if(!empty($userRecords))
							{
								$i=1;
								foreach($userRecords as $record)
								{
							?>
							<tr>
							  <td><?php echo $i++ ?></td>
							  <td><?php echo $record->nama_kategori ?></td>
							  <td class="text-center">
								  <a class="btn btn-sm btn-warning" title="Edit Kategori Buku" href="<?php echo base_url().'buku/editkategoribuku/'.$record->id_kategori; ?>"><i class="fa fa-pencil-square-o"></i></a>
								  <button type="button" name="delete" id="<?php echo $record->id_kategori; ?>" class="btn btn-sm btn-danger delete" title="Hapus Kategori Buku"><i class="fa fa-trash"></i></button>
							  </td>
							</tr>
							<?php
								}
							}
							?>
						  </tbody>
                        </table>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div>                
              </div><!-- /.col -->
			  <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
            </div><!-- /.row -->
          </section><!-- /.content -->
        <script type="text/javascript">
			$(document).ready(function() { var table = $('#mytable').DataTable(); } );
			
			$(document).on('click', '.delete', function(){  
			   var id_kategori = $(this).attr("id");  
			   if(confirm("Apakah Anda Yakin Ingin Menghapus Kategori Buku Ini?"))  
			   {  
					$.ajax({  
						 url:"<?php echo base_url('buku/hapuskategoribuku'); ?>",  
						 method:"POST",  
						 data:{id_kategori:id_kategori}  
					});
					var row = $(this).closest("tr");
					$('#mytable').DataTable().row(row).remove().draw();
			   }  
			   else  
			   {  
					return false;       
			   }  
			}); 
		</script>
</div>
