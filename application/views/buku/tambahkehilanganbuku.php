<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Tambah Kehilangan Buku
        <small><?= $keterangan ?></small>
      </h1>
    </section>    
    <section class="content">    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->               
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Masukkan Data <?= $keterangan ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="tambahBuku" action="<?php echo base_url('buku/tambahkehilanganbukubaru') ?>" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
										<label class="NIB">Buku Yang Hilang</label>
										<div class="">
											<select class="form-control select2" name="NIB" id="NIB">
												<option value="">Pilih Buku</option>
												<?php
												if(!empty($buku))
												{
													foreach($buku as $rs)
													{ ?><option value="<?= $rs->NIB ?>"><?= $rs->judul_buku ?> ( <?= $rs->ISBN ?> ) [ Stok : <?= $rs->jumlah ?> ]</option>
												<?php }} ?>
											</select><hr>
										</div>
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jumlah_hilang">Jumlah Hilang Buku</label>
										<input type="number" class="form-control required digits" id="jumlah_hilang" name="jumlah_hilang" min="1">
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tgl_hilang">Tanggal Hilang Buku</label>
										<input type="text" class="form-control required" id="tgl_hilang" name="tgl_hilang" maxlength="12">
                                    </div>
                                </div>
								<div class="col-md-12">
                                    <div class="form-group">
                                        <label for="keterangan">Keterangan Tambahan</label>
										<input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Kosongkan Jika Tidak Ada">
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    <script>
	  $(function () {
		$('#tgl_hilang').datepicker({
		  format: 'yyyy-mm-dd',
		  autoclose: true,
		});
	  });
	  $(function(){$(".select2").select2();});
	</script>
</div>
<script src="<?php echo base_url(); ?>assets/js/tambahkehilanganBuku.js" type="text/javascript"></script>