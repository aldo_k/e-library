<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content">
            <div class="row">
              <div class="col-xs-12">
			  <div class='box'>
                <div class='box-header'>
					<ol class="breadcrumb">
						<li><a href="<?=base_url()?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
						<li class="active"><?= $keterangan ?></li>
					</ol>
				</div>
				<form action="<?php echo base_url('buku/tambahpinjambukugurustaff') ?>" method="post" role="form" class="from-horizontal" id="tambahpinjambukugurustaff">
                    <div class="box-body">
						<div class="row"> 
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-12"><h3>Detail Informasi Buku </h3><hr></label>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">No. Induk Buku<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->NIB ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Kategori<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->nama_kategori ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Jenis Buku<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->jenis_buku ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Penerbit<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->penerbit ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Pengarang<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->pengarang ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">ISBN<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->ISBN ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Edisi<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->edisi ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Bahasa<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->bahasa ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Asal<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->asal ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Tanggal Masuk<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->tgl_masuk ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Tahun Terbit<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->tahun_terbit ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Stok Awal<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->jumlah ?><hr>
									</div>
                                </div>
                            </div>
							<!--// Awal Info Tambahan Mengenai Buku   -->
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Siswa<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										Meminjam <?= $pinjamsiswa=$jumlahpeminjamansiswa->total_buku-0 ?> Buku<hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Guru/Staff<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										Meminjam <?= $pinjamguru=$jumlahpeminjamangurustaff->total_buku-0 ?> Buku<hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Jumlah Hilang<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $pinjampetugas=$jumlahpeminjamanpetugas->jumlah_hilang-0 ?> Buku<hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Stock Akhir<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $stokakhir=($detailbuku->jumlah)-$pinjamguru-$pinjampetugas-$pinjamsiswa ?><hr>
									</div>
                                </div>
                            </div>
							<!--// Akhir Info Tambahan Mengenai Buku   -->
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Deskirpsi Buku<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										 <?= $detailbuku->deskripsi_buku ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-xs-3">Lokasi Buku<hr></label>
									<label class="control-label col-xs-1">:</label>
									<div class="col-xs-8">
										<?= $detailbuku->lokasi_buku ?><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-12"><hr><h3>Masukkan Data Peminjam</h3><hr></label>
                                </div>
                            </div>
                        </div>
						<div class="row">
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-md-4">Peminjam :<hr></label>
									<div class="col-md-8">
										<select class="form-control select2" name="no_identitas" id="no_identitas">
											<option value="">Pilih Peminjam ( Guru / Staff )</option><?php
										if(!empty($guru))
										{
											foreach($guru as $rs)
											{
										?><option value="<?= $rs->no_identitas ?>"><?= $rs->nama ?> ( <?= $rs->jabatan ?> ) </option>
										<?php }} ?></select><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-md-4">Jumlah Buku :<hr></label>
									<div class="col-md-8">
										<input type="number" class="form-control required digits" id="jumlah" name="jumlah" value="0" min="1" max="<?= $stokakhir ?>" ><hr>
									</div>
                                </div>
                            </div>
						</div>
						<div class="row">
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-md-4">Tanggal Pinjam :<hr></label>
									<div class="col-md-8">
										<input type="text" class="form-control required date" id="tgl_pinjam" name="tgl_pinjam" maxlength="32" placeholder="Masukkan Tanggal Melakukan Peminjaman"><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-md-4">Tanggal Kembali :<hr></label>
									<div class="col-md-8">
										<input type="text" class="form-control required date" id="tgl_harus_kembali" name="tgl_harus_kembali" maxlength="32"  placeholder="Masukkan Tanggal Kapan Mengembalikan Buku" ><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-12"><hr></label>
                                </div>
                            </div>
						</div>
                    </div>
                    <div class="box-footer">
						<input name="id" value="<?= $detailbuku->NIB ?>" type="hidden">
                        <input type="submit" class="btn btn-primary" value="Tambah <?= $keterangan ?>" />
                        <input type="reset" class="btn btn-default" value="Reset" />
                    </div>
                </form>
			  </div>
			<script src="<?php echo base_url(); ?>assets/js/tambahpinjambuku.js" type="text/javascript"></script>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?= $keterangan ?></a></li>
                  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                        <table id="mytable" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                          <thead>
                            <tr>
							  <th class="col-xs-1">No</th>
							  <th class="col-xs-2">Nama Peminjam</th>
							  <th class="col-xs-1">Status</th>
							  <th class="col-xs-1">Jabatan</th>
							  <th class="col-xs-2">Judul Buku</th>
							  <th class="col-xs-1">ISBN</th>
							  <th class="col-xs-1">Jumlah</th>
							  <th class="col-xs-1">Tanggal Pinjam</th>
							  <th class="col-xs-1">Batas Pengembalian</th>
							  <th class="col-xs-1">Aksi</th>
                            </tr>
                          </thead>
                          <tbody>	
							<?php
							if(!empty($peminjaman))
							{
								$i=1;
								foreach($peminjaman as $record)
								{
							?>
							<tr>
							  <td><?php echo $i++ ?></td>
							  <td><?php echo $record->nama ?></td>
							  <td><?php echo $record->status ?></td>
							  <td><?php echo $record->jabatan ?></td>
							  <td><?php echo $record->judul_buku ?></td>
							  <td><?php echo $record->ISBN ?></td>
							  <td><?php echo $record->total_buku ?></td>
							  <td><?php echo $record->tgl_pinjam ?></td>
							  <td><?php echo $record->tgl_harus_kembali ?></td>
							  <td class="text-center">
							      <?php if(($record->sudah_kembali_atau_belum)=="y"){
								  ?><a class="btn btn-sm btn-success" href="javascript:void(0)" title="Lihat Detail Peminjam" onclick="sudahkembali()"><i class="fa fa-info-circle"></i></a>
								  <button type="button" name="delete" id="<?php echo $record->id_peminjaman; ?>" class="btn btn-sm btn-danger delete" title="Hapus Peminjaman Buku"><i class="fa fa-trash"></i></button>
								  <?php } else { ?>
								  <a class="btn btn-sm btn-info" title="Pengembalian Buku" href="<?php echo base_url().'buku/pengembalianbuku/'.$record->id_peminjaman; ?>/<?= $record->NIP; ?>"><i class="fa fa-plus"></i></a> 
								  <button type="button" id="" class="btn btn-sm btn-danger" title="Belum Dikembalikan" onclick="status()"><i class="fa fa-minus-circle"></i></button>
								  <?php } ?>
								  <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Lihat Detail Buku" onclick="detail_buku('<?php echo $record->NIB; ?>')"><i class="fa fa-eye"></i></a>
								  <a class="btn btn-sm btn-info" href="javascript:void(0)" title="Lihat Detail Peminjam" onclick="detail_peminjam('<?php echo $record->NIP; ?>')"><i class="fa fa-eye"></i></a>
							  </td>
							</tr>
							<?php
								}
							}
							?>
						  </tbody>
                        </table>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div>                
              </div><!-- /.col -->
            </div><!-- /.row -->
          </section><!-- /.content -->
        <script type="text/javascript">
			$(document).ready(function() { var table = $('#mytable').DataTable(); } );
			
			function detail_peminjam(id)
			{
				save_method = 'update';
				$('#form')[0].reset();
				$('.form-group').removeClass('has-error');
				$('.help-block').empty();
			 
				$.ajax({
					url : "<?php echo site_url('user/Detailgurustaff/')?>/" + id,
					type: "GET",
					dataType: "JSON",
					success: function(data)
					{
			 
						$('.no_identitas').text(data.no_identitas);
						$('.nama').text(data.nama);
						$('.tempat_lahir').text(data.tempat_lahir);
						$('.tgl_lahir').text(data.tgl_lahir);
						$('.alamat').text(data.alamat);
						$('.telepon').text(data.telepon);
						$('.username').text(data.username);
						$('.status').text(data.status);
						$('.jabatan').text(data.jabatan);
						$('.foto').text(data.foto);
						if((data.jenis_kelamin)='L'){ $('.jenis_kelamin').text('Laki-Laki'); }
							else if((data.jenis_kelamin)='P') { $('.jenis_kelamin').text('Perempuan'); }
							else { $('.jenis_kelamin').text('Jenis Kelamin Tidak Diketahui'); }
						
						$('#modal_form').modal('show');
						$('.modal-title').text('Detail Data Guru / Staff');
			 
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error get data from ajax');
					}
				});
			}
			
			function status(){alert('Tidak Dapat Melakukan Tindakan Ini Dikarenakan Peminjam Belum Mengembalikan Buku.');}
			
			function sudahkembali()
			{
				$('#form')[0].reset();
				$('.form-group').removeClass('has-error');
				$('.help-block').empty();
				$('#sudahkembali').modal('show');
			}
			
		$(function(){$(".select2").select2();});
		$(function () {
		$('.date').datepicker({
		  format: 'yyyy-mm-dd',
		  autoclose: true,
			});
		  });
        </script>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">NIP</label>
                            <div class="col-md-9 no_identitas">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Lengkap</label>
                            <div class="col-md-9 nama">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tempat Lahir</label>
                            <div class="col-md-9 tempat_lahir">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal Lahir</label>
                            <div class="col-md-9 tgl_lahir">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Telepon</label>
                            <div class="col-md-9 telepon">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Jenis Kelamin</label>
                            <div class="col-md-9 jenis_kelamin">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Jabatan</label>
                            <div class="col-md-9 jabatan">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9 status">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Alamat</label>
                            <div class="col-md-9 alamat">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Username</label>
                            <div class="col-md-9 username">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Foto</label>
                            <div class="col-md-9 foto">
                                
                            </div>
                        </div>
						
                    </div>
					</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

<!-- Bootstrap modal -->
<div class="modal fade" id="sudahkembali" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Buku Yg Dipinjam Sudah Dikembalikan</h3>
            </div>
            <div class="modal-body form">
                Peminjam Ini Sudah Mengembalikan Buku Yg Dipinjamnya...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Oke</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->