<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content">
            <div class="row">
              <div class="col-xs-12">
			  <div class='box'>
                <div class='box-header'>
					<ol class="breadcrumb">
						<li><a href="<?=base_url()?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
						<li class="active"><?= $keterangan ?></li>
					</ol>
				</div>
			  </div>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?= $keterangan ?></a></li>
                  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                        <table id="mytable" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                          <thead>
                            <tr>
							  <th>ID Transaksi</th>
							  <th class="col-xs-2">Nama Peminjam</th>
							  <th class="col-xs-1">Kelas</th>
							  <th class="col-xs-2">Judul Buku</th>
							  <th class="col-xs-1">ISBN</th>
							  <th class="col-xs-1">Tanggal Pinjam</th>
							  <th class="col-xs-1">ID Peminjaman</th>
							  <th>Jumlah</th>
							  <th class="col-xs-2">Aksi</th>
                            </tr>
                          </thead>
                          <tbody>	
							<?php
							if(!empty($peminjaman))
							{
								foreach($peminjaman as $record)
								{
							?>
							<tr>
							  <td><?php echo $record->id_transaksi ?></td>
							  <td><?php echo $record->nama ?></td>
							  <td><?php echo $record->nama_kelas ?></td>
							  <td><?php echo $record->judul_buku ?></td>
							  <td><?php echo $record->ISBN ?></td>
							  <td><?php echo $record->tgl_pinjam ?></td>
							  <td><?php echo $record->id_peminjaman ?></td>
							  <td><?php echo $record->total_buku ?></td>
							  <td class="text-center">
							      <?php if(($record->sudah_kembali_atau_belum)=="y"){
								  ?><a class="btn btn-sm btn-success" href="javascript:void(0)" title="Lihat Detail Peminjam" onclick="sudahkembali()"><i class="fa fa-info-circle"></i></a>
								  <button type="button" name="delete" id="<?php echo $record->id_peminjaman; ?>" class="btn btn-sm btn-danger delete" title="Hapus Peminjaman Buku"><i class="fa fa-trash"></i></button>
								  <?php } else { ?>
								  <a class="btn btn-sm btn-info" title="Pengembalian Buku" href="<?php echo base_url().'buku/pengembalianbuku/'.$record->id_peminjaman; ?>/<?= $record->NIS; ?>"><i class="fa fa-plus"></i></a> 
								  <button type="button" id="" class="btn btn-sm btn-danger" title="Belum Dikembalikan" onclick="status()"><i class="fa fa-minus-circle"></i></button>
								  <?php } ?>
								  <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Lihat Detail Buku" onclick="detail_buku('<?php echo $record->NIB; ?>')"><i class="fa fa-eye"></i></a>
								  <a class="btn btn-sm btn-info" href="javascript:void(0)" title="Lihat Detail Peminjam" onclick="detail_peminjam('<?php echo $record->NIS; ?>')"><i class="fa fa-eye"></i></a>
							  </td>
							</tr>
							<?php
								}
							}
							?>
						  </tbody>
                        </table>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div>                
              </div><!-- /.col -->
            </div><!-- /.row -->
          </section><!-- /.content -->
        <script type="text/javascript">
			$(document).ready(function() { var table = $('#mytable').DataTable(); } );
			
			$(document).on('click', '.delete', function(){  
			   var id_peminjaman = $(this).attr("id");  
			   if(confirm("Apakah Anda Yakin Ingin Menghapus Peminjaman Ini?"))  
			   {  
					$.ajax({  
						 url:"<?php echo base_url('buku/deletepeminjamanbuku'); ?>",  
						 method:"POST",  
						 data:{id_peminjaman:id_peminjaman}  
					});
					var row = $(this).closest("tr");
					$('#mytable').DataTable().row(row).remove().draw();
			   }  
			   else  
			   {  
					return false;       
			   }  
			}); 
			
			function detail_peminjam(id)
			{
				save_method = 'update';
				$('#form')[0].reset();
				$('.form-group').removeClass('has-error');
				$('.help-block').empty();
			 
				$.ajax({
					url : "<?php echo site_url('user/Detailsiswa/')?>/" + id,
					type: "GET",
					dataType: "JSON",
					success: function(data)
					{
			 
						$('.no_identitas').text(data.no_identitas);
						$('.nama').text(data.nama);
						$('.tempat_lahir').text(data.tempat_lahir);
						$('.tgl_lahir').text(data.tgl_lahir);
						$('.alamat').text(data.alamat);
						$('.telepon').text(data.telepon);
						$('.username').text(data.username);
						$('.jurusan').text(data.jurusan);
						$('.kelas').text(data.kelas);
						$('.foto').attr('src', '<?= base_url('assets/foto/siswa')?>/'+data.foto);
						if((data.jenis_kelamin)='L'){ $('.jenis_kelamin').text('Laki-Laki'); }
							else if((data.jenis_kelamin)='P') { $('.jenis_kelamin').text('Perempuan'); }
							else { $('.jenis_kelamin').text('Jenis Kelamin Tidak Diketahui'); }
						
						$('#modal_formPeminjam').modal('show');
						$('.modal-title').text('Detail Data Peminjam');
			 
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error get data from ajax');
					}
				});
			}
			
			function status(){alert('Tidak Dapat Melakukan Tindakan Ini Dikarenakan Peminjam Belum Mengembalikan Buku.');}
			
			function sudahkembali()
			{
				$('#form')[0].reset();
				$('.form-group').removeClass('has-error');
				$('.help-block').empty();
				$('#sudahkembali').modal('show');
			}
			
			function detail_buku(id)
			{
				save_method = 'update';
				$('#form')[0].reset();
				$('.form-group').removeClass('has-error');
				$('.help-block').empty();
			 
				$.ajax({
					url : "<?php echo site_url('buku/Detailbuku/')?>/" + id,
					type: "GET",
					dataType: "JSON",
					success: function(data)
					{
			 
						$('.NIB').text(data.NIB);
						$('.nama_kategori').text(data.nama_kategori);
						$('.jenis_buku').text(data.jenis_buku);
						$('.judul_buku').text(data.judul_buku);
						$('.penerbit').text(data.penerbit);
						$('.pengarang').text(data.pengarang);
						$('.bukuname').text(data.bukuname);
						$('.jumlah').text(data.jumlah);
						$('.ISBN').text(data.ISBN);
						$('.edisi').text(data.edisi);
						$('.bahasa').text(data.bahasa);
						$('.asal').text(data.asal);
						$('.tgl_masuk').text(data.tgl_masuk);
						$('.tahun_terbit').text(data.tahun_terbit);
						$('.deskripsi_buku').text(data.deskripsi_buku);
						$('.lokasi_buku').text(data.lokasi_buku);
						$('.tahun_terbit').text(data.tahun_terbit);
						$('#modal_formBuku').modal('show');
						$('.modal-title').text('Detail Data Buku');
			 
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error get data from ajax');
					}
				});
			}
        </script>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_formPeminjam" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">NIS</label>
                            <div class="col-md-9 no_identitas">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Lengkap</label>
                            <div class="col-md-9 nama">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tempat Lahir</label>
                            <div class="col-md-9 tempat_lahir">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal Lahir</label>
                            <div class="col-md-9 tgl_lahir">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Telepon</label>
                            <div class="col-md-9 telepon">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Jenis Kelamin</label>
                            <div class="col-md-9 jenis_kelamin">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Jurusan</label>
                            <div class="col-md-9 jurusan">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Kelas</label>
                            <div class="col-md-9 kelas">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Alamat</label>
                            <div class="col-md-9 alamat">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Username</label>
                            <div class="col-md-9 username">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Foto</label>
                            <div class="col-md-9">
                                <img src="" width="340px" class="foto">
                            </div>
                        </div>
						
                    </div>
					</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_formBuku" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">NIB :</label>
                            <div class="col-md-9 NIB">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kategori :</label>
                            <div class="col-md-9 nama_kategori">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Jenis Buku :</label>
                            <div class="col-md-9 jenis_buku">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Penerbit :</label>
                            <div class="col-md-9 penerbit">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Pengarang :</label>
                            <div class="col-md-9 pengarang">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Jumlah Buku :</label>
                            <div class="col-md-9 jumlah">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">ISBN :</label>
                            <div class="col-md-9 ISBN">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Edisi :</label>
                            <div class="col-md-9 edisi">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Bahasa :</label>
                            <div class="col-md-9 bahasa">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Asal :</label>
                            <div class="col-md-9 asal">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Tanggal Masuk :</label>
                            <div class="col-md-9 tgl_masuk">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Tahun Terbit :</label>
                            <div class="col-md-9 tahun_terbit">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Deskripsi Buku :</label>
                            <div class="col-md-9 deskripsi_buku">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Lokasi Buku :</label>
                            <div class="col-md-9 lokasi_buku">
                                
                            </div>
                        </div>
						
                    </div>
					</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

<!-- Bootstrap modal -->
<div class="modal fade" id="sudahkembali" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Buku Yg Dipinjam Sudah Dikembalikan</h3>
            </div>
            <div class="modal-body form">
                Peminjam Ini Sudah Mengembalikan Buku Yg Dipinjamnya...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Oke</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->