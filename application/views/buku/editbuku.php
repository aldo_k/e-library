<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Edit Buku
        <small><?= $keterangan ?></small>
      </h1>
    </section>    
    <section class="content">    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->               
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Masukkan Data <?= $keterangan ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="editBuku" action="<?php echo base_url('buku/editbukunya') ?>" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="NIB">No Induk Buku</label>
                                        <input type="text" class="form-control required NIB" id="NIB"  name="NIB" maxlength="32" value="<?= $NIB; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="kategori">Kategori Buku</label>
                                        <select class="form-control required" id="kategori" name="kategori">
                                            <option value="">Pilih Kategori Buku</option>
											<?php
											if(!empty($kategori))
											{
												$i=1;
												foreach($kategori as $record)
												{
											?><option value="<?= $record->id_kategori;?>"><?= $record->nama_kategori;?></option>
											<?php }} ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jenis_buku">Jenis Buku</label>
                                        <select class="form-control required" id="jenis_buku" name="jenis_buku">
                                            <option value="">Pilih Jenis Buku</option>
											<option value="Buku ( UMUM )">Buku ( UMUM )</option>
											<option value="Koleksi Referens">Koleksi Referens</option>
											<option value="Sumber Geografis">Sumber Geografis</option>
											<option value="Jenis Serial">Jenis Serial</option>
											<option value="Bahan Mikro">Bahan Mikro</option>
											<option value="Bahan Pandang Dengar ( Audio Visual )">Bahan Pandang Dengar ( Audio Visual )</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="ISBN">ISBN Buku</label>
                                        <input type="text" class="form-control required" id="ISBN" name="ISBN" maxlength="24" value="<?= $ISBN; ?>">
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="judul_buku">Judul Buku</label>
                                        <input type="text" class="form-control required " id="judul_buku" name="judul_buku" maxlength="128" value="<?= $judul_buku; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="penerbit">Penerbit</label>
                                        <input type="text" class="form-control required " id="penerbit" name="penerbit" maxlength="64" value="<?= $penerbit; ?>">
                                    </div>
                                </div> 
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pengarang">Pengarang</label>
                                        <input type="text" class="form-control required " id="pengarang" name="pengarang" maxlength="64" value="<?= $pengarang; ?>">
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="jumlah">Jumlah Buku</label>
										<input type="number" class="form-control required digits" id="jumlah" name="jumlah" min="1" value="<?= $jumlah; ?>">
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label for="edisi">Edisi Buku</label>
										<input type="text" class="form-control required" id="edisi" name="edisi" maxlength="12" value="<?= $edisi ?>">
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label for="bahasa">Bahasa Buku</label>
										<input type="text" class="form-control required" id="bahasa" name="bahasa" maxlength="24" value="<?= $bahasa ?>">
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="asal">Asal Buku</label>
                                        <input type="text" class="form-control required" id="asal" name="asal" maxlength="32" value="<?= $asal ?>">
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label for="tgl_masuk">Tanggal Masuk</label>
                                        <input type="text" class="form-control required" id="tgl_masuk" name="tgl_masuk" maxlength="10" value="<?= $tgl_masuk; ?>">
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label for="tahun_terbit">Tahun Terbit</label>
										<input type="number" class="form-control required" id="tahun_terbit" name="tahun_terbit" min="1900" max="<?= date('Y'); ?>" value="<?= $tahun_terbit; ?>">
                                    </div>
                                </div>  
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="deskripsi_buku">Deskripsi Buku</label>
                                        <textarea class="form-control" rows="3" placeholder="Deskripsi Buku..." name="deskripsi_buku" id="deskripsi_buku"><?= $deskripsi_buku; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lokasi_buku">Lokasi Buku</label>
                                        <textarea class="form-control" rows="3" placeholder="Lokasi Buku..." name="lokasi_buku" id="lokasi_buku"><?= $lokasi_buku; ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
							<input type="hidden" class="" id="id"  name="id" value="<?= $NIB; ?>">
							<input type="hidden" class="" id="idISBN"  name="idISBN" value="<?= $ISBN; ?>">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    <script>
	  $(function () {
		$('#tgl_masuk').datepicker({
		  format: 'yyyy-mm-dd',
		  autoclose: true,
		});
	  });
	$('#kategori').val('<?= $id_kategori; ?>');
	$('#jenis_buku').val('<?= $jenis_buku; ?>');
	</script>
</div>
<script src="<?php echo base_url(); ?>assets/js/editBuku.js" type="text/javascript"></script>