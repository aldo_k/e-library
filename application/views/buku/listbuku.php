<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content">
            <div class="row">
              <div class="col-xs-12">
			  <div class='box'>
                <div class='box-header'>
					<ol class="breadcrumb">
						<li><a href="<?=base_url()?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
						<li class="active"><?= $keterangan ?></li>
					</ol>
				</div>
			  </div>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?= $keterangan ?></a></li>
                  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
					<?php if($usertype == ROLE_ADMIN){ ?><h3><?php echo anchor('buku/Tambahbuku','<i class="fa fa-plus"></i> Tambah '.$keterangan,array('class'=>'btn btn-primary btn-sm'));?></h3><?php } ?>
                        <table id="mytable" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                          <thead>
                            <tr>
							  <th class="col-xs-1">No</th>
							  <th class="col-xs-3">Judul Buku</th>
							  <th class="col-xs-2">Penerbit</th>
							  <th class="col-xs-2">Pengarang</th>
							  <th class="col-xs-1">ISBN</th>
							  <th class="col-xs-1">Jumlah Buku</th>
								<th class="col-xs-2">Aksi</th>
                            </tr>
                          </thead>
                          <tbody>	
							<?php
							if(!empty($bukuRecords))
							{
								$i=1;
								foreach($bukuRecords as $record)
								{
							?>
							<tr>
							  <td><?php echo $i++ ?></td>
							  <td><?php echo $record->judul_buku ?></td>
							  <td><?php echo $record->penerbit ?></td>
							  <td><?php echo $record->pengarang ?></td>
							  <td><?php echo $record->ISBN ?></td>
							  <td><?php echo $record->jumlah-$record->jumlahbuku-$record->jumlahbukuhilang ?> (<?php echo $selisih=$record->jumlah-$record->jumlahbuku-$record->jumlahbukuhilang ?>/<?php echo $record->jumlah ?>)</td><?php if($usertype == ROLE_ADMIN){ ?>
							  <td class="text-center">
								  <?php if($selisih==0){ ?><button type="button" name="" class="btn btn-sm btn-warning" title="Tambah Peminjaman Buku" onclick="javascript: alert('Tidak Dapat Melakukan Peminjaman Dikarenakan Stock Buku Habis !!');"><i class="fa fa-plus"></i></button><?php } else { ?><a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Tambah Peminjaman Buku" onclick="tambahpeminjamanbuku('<?php echo $record->NIB; ?>')"><i class="fa fa-plus"></i></a><?php } ?>
								  <a class="btn btn-sm btn-info" href="javascript:void(0)" title="Lihat Detail Buku" onclick="detail_buku('<?php echo $record->NIB; ?>')"><i class="fa fa-eye"></i></a>
								  <a class="btn btn-sm btn-warning" title="Edit Buku" href="<?php echo base_url().'buku/EditBuku/'.$record->NIB; ?>"><i class="fa fa-pencil-square-o"></i></a><?php 
								  if (($record->jumlah-$record->jumlahbuku)==$record->jumlah) {
								  ?>
								  <button type="button" name="delete" id="<?php echo $record->NIB; ?>" class="btn btn-sm btn-danger delete" title="Hapus Buku"><i class="fa fa-trash"></i></button><?php } else { ?>
								  <button type="button" name="" class="btn btn-sm btn-danger" title="Hapus Buku" onclick="javascript: alert('Buku Tidak Bisa Dihapus Karena Masih Ada Yg Belum Mengembalikan Buku');"><i class="fa fa-trash"></i></button>
								  <?php } ?>
							  </td><?php } ?>
							  <?php if($usertype != ROLE_ADMIN){ ?>
							  <td class="text-center">
								  <a class="btn btn-sm btn-danger" title="Lihat Stok Buku" href="<?php echo base_url().'buku/InformasiBuku/'.$record->NIB; ?>"><i class="fa fa-eye"></i> Stok</a>
								  <a class="btn btn-sm btn-info" href="javascript:void(0)" title="Lihat Detail Buku" onclick="detail_buku('<?php echo $record->NIB; ?>')"><i class="fa fa-eye"></i> Detail</a>
							  </td>
							  <?php } ?>
							</tr>
							<?php
								}
							}
							?>
						  </tbody>
                        </table>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div>                
              </div><!-- /.col -->
            </div><!-- /.row -->
          </section><!-- /.content -->
        <script type="text/javascript">
			$(document).ready(function() { var table = $('#mytable').DataTable(); } );
			<?php if($usertype == ROLE_ADMIN){ ?>
			$(document).on('click', '.delete', function(){  
			   var NIB = $(this).attr("id");  
			   if(confirm("Apakah Anda Yakin Ingin Menghapus Buku Ini?"))  
			   {  
					$.ajax({  
						 url:"<?php echo base_url('buku/deleteBuku'); ?>",  
						 method:"POST",  
						 data:{NIB:NIB}  
					});
					var row = $(this).closest("tr");
					$('#mytable').DataTable().row(row).remove().draw();
			   }  
			   else  
			   {  
					return false;       
			   }  
			}); 
			<?php } ?>
			function detail_buku(id)
			{
				save_method = 'update';
				$('#form')[0].reset();
				$('.form-group').removeClass('has-error');
				$('.help-block').empty();
			 
				$.ajax({
					url : "<?php echo site_url('buku/Detailbuku/')?>/" + id,
					type: "GET",
					dataType: "JSON",
					success: function(data)
					{
			 
						$('.NIB').text(data.NIB);
						$('.nama_kategori').text(data.nama_kategori);
						$('.jenis_buku').text(data.jenis_buku);
						$('.judul_buku').text(data.judul_buku);
						$('.penerbit').text(data.penerbit);
						$('.pengarang').text(data.pengarang);
						$('.bukuname').text(data.bukuname);
						$('.jumlah').text(data.jumlah);
						$('.ISBN').text(data.ISBN);
						$('.edisi').text(data.edisi);
						$('.bahasa').text(data.bahasa);
						$('.asal').text(data.asal);
						$('.tgl_masuk').text(data.tgl_masuk);
						$('.tahun_terbit').text(data.tahun_terbit);
						$('.deskripsi_buku').text(data.deskripsi_buku);
						$('.lokasi_buku').text(data.lokasi_buku);
						$('.tahun_terbit').text(data.tahun_terbit);
						$('#modal_form').modal('show');
						$('.modal-title').text('Detail Data Buku');
			 
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error get data from ajax');
					}
				});
			}
			<?php if($usertype == ROLE_ADMIN){ ?>
			function tambahpeminjamanbuku(id)
			{
				$('#form')[0].reset();
				$('.form-group').removeClass('has-error');
				$('.help-block').empty();
				$("a#siswa").attr("href", "<?= base_url('buku/pinjambukusiswa/') ?>"+"/"+id);
				$("a#gurustaff").attr("href", "<?= base_url('buku/pinjambukugurustaff/') ?>"+"/"+id);
				$("a#petugas").attr("href", "<?= base_url('buku/pinjambukupetugas/') ?>"+"/"+id);
				$('#tambahpeminjamanbuku').modal('show');
			}
			<?php } ?>
        </script>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">NIB :</label>
                            <div class="col-md-9 NIB">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kategori :</label>
                            <div class="col-md-9 nama_kategori">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Jenis Buku :</label>
                            <div class="col-md-9 jenis_buku">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Penerbit :</label>
                            <div class="col-md-9 penerbit">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Pengarang :</label>
                            <div class="col-md-9 pengarang">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Jumlah Buku :</label>
                            <div class="col-md-9 jumlah">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">ISBN :</label>
                            <div class="col-md-9 ISBN">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Edisi :</label>
                            <div class="col-md-9 edisi">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Bahasa :</label>
                            <div class="col-md-9 bahasa">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Asal :</label>
                            <div class="col-md-9 asal">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Tanggal Masuk :</label>
                            <div class="col-md-9 tgl_masuk">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Tahun Terbit :</label>
                            <div class="col-md-9 tahun_terbit">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Deskripsi Buku :</label>
                            <div class="col-md-9 deskripsi_buku">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Lokasi Buku :</label>
                            <div class="col-md-9 lokasi_buku">
                                
                            </div>
                        </div>
						
                    </div>
					</form>
            </div>
            <div class="modal-footer">
			<?php if($usertype == ROLE_ADMIN){ ?><a class="btn btn-sm btn-warning" title="Edit Buku" href="<?php echo base_url().'buku/Editbuku/'.$record->NIB; ?>"><i class="fa fa-pencil-square-o"></i> Edit Buku</a><?php } ?>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->
<?php if($usertype == ROLE_ADMIN){ ?>
<!-- Bootstrap modal -->
<div class="modal fade" id="tambahpeminjamanbuku" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Pilih Peminjam Buku</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
						<div class="form-group">
                            <label class="control-label col-md-6">Tambah Peminjaman Buku Untuk :</label>
                            <div class="col-md-6">
							<a class="btn btn-sm btn-danger" title="Tambah Peminjaman Buku Siswa" id="siswa" href=""><i class="fa fa-plus"></i> Para Siswa</a>    
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-6">Tambah Peminjaman Buku Untuk :</label>
                            <div class="col-md-6">
							<a class="btn btn-sm btn-danger" title="Tambah Peminjaman Buku Guru / Staff" id="gurustaff" href=""><i class="fa fa-plus"></i> Para Guru / Staff</a>    
                            </div>
                        </div>
                    </div>
					</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->
<?php } ?>