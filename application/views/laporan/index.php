<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content">
            <div class="row">
              <div class="col-xs-12">
			  <div class='box'>
                <div class='box-header'>
					<ol class="breadcrumb">
						<li><a href="<?=base_url()?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
						<li class="active"><?= $keterangan ?></li>
					</ol>
				</div>
			  </div>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?= $keterangan ?></a></li>
                  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                    <div class="box-body">
						<div class="row"> 
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-4">Cetak Laporan Data Buku<hr></label>
									<div class="col-xs-8">
										: <a class="btn btn-sm btn-success" title="Cetak Laporan Data Buku" href="<?php echo base_url().'laporan/buku'?>"><i class="fa fa-file-pdf-o"></i> Cetak Data Buku</a><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-4">Cetak Laporan Data Anggota Siswa<hr></label>
									<div class="col-xs-8">
										: <a class="btn btn-sm btn-success" title="Cetak Laporan Data Siswa" href="<?php echo base_url().'laporan/siswa'?>"><i class="fa fa-file-pdf-o"></i> Cetak Data Siswa</a><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-4">Cetak Laporan Data Anggota Guru/Staff<hr></label>
									<div class="col-xs-8">
										: <a class="btn btn-sm btn-success" title="Cetak Laporan Data Guru/Staff" href="<?php echo base_url().'laporan/gurustaff'?>"><i class="fa fa-file-pdf-o"></i> Cetak Data Guru/Staff</a><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-4">Cetak Laporan Data Kehilangan Buku<hr></label>
									<div class="col-xs-8">
										: <a class="btn btn-sm btn-success" title="Cetak Laporan Data Kehilangan Buku" href="<?php echo base_url().'laporan/kehilanganbuku'?>"><i class="fa fa-file-pdf-o"></i> Cetak Data Kehilangan Buku</a><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-4">Cetak Laporan Data Peminjaman Buku Siswa<hr></label>
									<div class="col-xs-8">
										: <a class="btn btn-sm btn-success" title="Cetak Laporan Data Peminjaman Buku" href="<?php echo base_url().'laporan/peminjamansiswa'?>"><i class="fa fa-file-pdf-o"></i> Cetak Data Peminjaman Buku Siswa</a><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-4">Cetak Laporan Data Peminjaman Buku Guru/Staff<hr></label>
									<div class="col-xs-8">
										: <a class="btn btn-sm btn-success" title="Cetak Laporan Data Peminjaman Buku" href="<?php echo base_url().'laporan/peminjamangurustaff'?>"><i class="fa fa-file-pdf-o"></i> Cetak Data Peminjaman Buku Guru/Staff</a><hr>
									</div>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="row">
                                    <label class="control-label col-xs-4">Cetak Laporan Data Pengembalian Buku<hr></label>
									<div class="col-xs-8">
										: <a class="btn btn-sm btn-success" title="Cetak Laporan Data Pengembalian Buku" href="<?php echo base_url().'laporan/pengembalian'?>"><i class="fa fa-file-pdf-o"></i> Cetak Data Pengembalian Buku</a><hr>
									</div>
                                </div>
                            </div>
						</div>
					</div>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div>                
              </div><!-- /.col -->
            </div><!-- /.row -->
          </section><!-- /.content -->
	</div>