<html>
<head>
<style>
html { margin: 10px}
@page { margin: 10px; size: 21cm 29.7cm; }
body { margin: 10px; }
th, td 
{
padding: 5px;
}
th
{
	background-color:#f0f0f0;
}
</style>
   </head>
	<body>
	<table align="center" border="1" style="border-collapse:collapse; padding:5px;">
		<thead style="display:table-header-group;">
			<tr>
							  <th>Nama Peminjam</th>
							  <th>Status</th>
							  <th>Judul Buku</th>
							  <th>ISBN</th>
							  <th>Jumlah</th>
							  <th>Tanggal Pinjam</th>
							  <th>Batas Kembali</th>
							  <th>Keterangan</th>
                            </tr>
                          </thead>
                          <tbody>	
							<?php
							if(!empty($peminjaman))
							{
								foreach($peminjaman as $record)
								{
							?>
							<tr>
							  <td><?php echo $record->nama ?></td>
							  <td><?php echo $record->status ?></td>
							  <td><?php echo $record->judul_buku ?></td>
							  <td><?php echo $record->ISBN ?></td>
							  <td><?php echo $record->total_buku ?></td>
							  <td><?php echo $record->tgl_pinjam ?></td>
							  <td><?php echo $record->tgl_harus_kembali ?></td>
							  <td><?php if ($record->sudah_kembali_atau_belum=="y"){ echo "Sudah Kembali";} else { echo "Belum Kembali";} ?></td>
							</tr>
							<?php
								}
							}
							?>
						  </tbody>
                        </table>
	</table>
	</body>
</html>