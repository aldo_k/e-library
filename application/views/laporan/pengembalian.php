<html>
<head>
<style>
html { margin: 10px}
@page { margin: 10px; size: 21cm 29.7cm; }
body { margin: 10px; }
th, td 
{
padding: 5px;
}
th
{
	background-color:#f0f0f0;
}
</style>
   </head>
	<body>
	<table align="center" border="1" style="border-collapse:collapse; padding:5px;">
		<thead style="display:table-header-group;">
			<tr>
							  <th>Id Pinjam</th>
							  <th>Judul Buku</th>
							  <th>Kembali</th>
							  <th>Denda</th>
							  <th>Sisa Denda</th>
							  <th>Tanggal Kembali</th>
							  <th>Keterangan</th>
                            </tr>
                          </thead>
                          <tbody>	
							<?php
							if(!empty($pengembalian))
							{
								foreach($pengembalian as $record)
								{
							?>
							<tr>
							  <td><?php echo $record->id_peminjaman ?></td>
							  <td><?php echo $record->judul_buku ?></td>
							  <td><?php echo $record->total_buku_dikembalikan ?></td>
							  <td><?php echo $jumlahdenda=$record->denda_keterlambatan+$record->denda_kerusakan_buku+$record->denda_kehilangan_buku ?></td>
							  <td><?php echo $record->jumlah_denda_dibayarkan-$jumlahdenda ?></td>
							  <td><?php echo $record->tgl_buku_dikembalikan ?></td>
							  <td><?php echo $record->keterangan ?></td>
							</tr>
							<?php
								}
							}
							?>
						  </tbody>
                        </table>
	</table>
	</body>
</html>