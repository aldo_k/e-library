<html>
<head>
<style>
html { margin: 10px}
@page { margin: 10px; size: 21cm 29.7cm; }
body { margin: 10px; }
th, td 
{
padding: 5px;
}
th
{
	background-color:#f0f0f0;
}
</style>
   </head>
	<body>
	<table align="center" border="1" style="border-collapse:collapse; padding:5px;">
		<thead style="display:table-header-group;">
			<tr>
							  <th>No</th>
							  <th>Judul Buku</th>
							  <th>Penerbit</th>
							  <th>Pengarang</th>
							  <th>ISBN</th>
							  <th>Jumlah Buku</th>
                            </tr>
                          </thead>
                          <tbody>	
							<?php
							if(!empty($bukuRecords))
							{
								$i=1;
								foreach($bukuRecords as $record)
								{
							?>
							<tr>
							  <td><?php echo $i++ ?></td>
							  <td><?php echo $record->judul_buku ?></td>
							  <td><?php echo $record->penerbit ?></td>
							  <td><?php echo $record->pengarang ?></td>
							  <td><?php echo $record->ISBN ?></td>
							  <td><?php echo $record->jumlah-$record->jumlahbuku-$record->jumlahbukuhilang ?> (<?php echo $selisih=$record->jumlah-$record->jumlahbuku-$record->jumlahbukuhilang ?>/<?php echo $record->jumlah ?>)</td>
							</tr>
							<?php
								}
							}
							?>
						  </tbody>
                        </table>
	</table>
	</body>
</html>