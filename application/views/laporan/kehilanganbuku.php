<html>
<head>
<style>
html { margin: 10px}
@page { margin: 10px; size: 21cm 29.7cm; }
body { margin: 10px; }
th, td 
{
padding: 5px;
}
th
{
	background-color:#f0f0f0;
}
</style>
   </head>
	<body>
	<table align="center" border="1" style="border-collapse:collapse; padding:5px;">
		<thead style="display:table-header-group;">
			<tr>
							  <th>No</th>
							  <th>Judul Buku</th>
							  <th>ISBN</th>
							  <th>Jumlah Hilang</th>
							  <th>Tanggal Hilang</th>
							  <th>Keterangan</th>
							  <th>Petugas</th>
                            </tr>
                          </thead>
                          <tbody>	
							<?php
							if(!empty($bukuRecords))
							{
								$i=1;
								foreach($bukuRecords as $record)
								{
							?>
							<tr>
							  <td><?php echo $i++ ?></td>
							  <td><?php echo $record->judul_buku ?></td>
							  <td><?php echo $record->ISBN ?></td>
							  <td><?php echo $record->jumlah_hilang ?></td>
							  <td><?php echo $record->tgl_hilang ?></td>
							  <td><?php echo $record->keterangan ?></td>
							  <td><?php echo $record->nama ?></td>
							</tr>
							<?php
								}
							}
							?>
						  </tbody>
                        </table>
	</table>
	</body>
</html>