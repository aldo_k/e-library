<html>
<head>
<style>
html { margin: 10px}
@page { margin: 10px; size: 21cm 29.7cm; }
body { margin: 10px; }
th, td 
{
padding: 5px;
}
th
{
	background-color:#f0f0f0;
}
</style>
   </head>
	<body>
	<table align="center" border="1" style="border-collapse:collapse; padding:5px;">
		<thead style="display:table-header-group;">
			<tr>
				<tr>
					<th>No</th>
					<th>Nama Lengkap</th>
					<th>NIP</th>
					<th>Status</th>
					<th>Jabatan</th>
					<th>Telepon</th>
					<th>Alamat</th>
			 </tr>
		 </thead>
		 <tbody>
			<?php
			if(!empty($userRecords))
			{
			$i=1;
			foreach($userRecords as $record)
			{
			?><tr>
				<td><?php echo $i++ ?></td>
				<td><?php echo $record->nama?></td>
				<td><?php echo $record->no_identitas ?></td>
				<td><?php echo $record->status ?></td>
				<td><?php echo $record->jabatan ?></td>
				<td><?php echo $record->telepon ?></td>
				<td><?php echo $record->alamat ?></td>
			</tr><?php }} ?>
		 </tbody>
	</table>
	</body>
</html>