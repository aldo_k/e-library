<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content">
            <div class="row">
              <div class="col-xs-12">
			  <div class='box'>
                <div class='box-header'>
					<ol class="breadcrumb">
						<li><a href="<?=base_url()?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
						<li class="active"><?= $keterangan ?></li>
					</ol>
				</div>
			  </div>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?= $keterangan ?></a></li>
                  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
					<h3><?php echo anchor('kelas/tambahkelas','<i class="fa fa-plus"></i> Tambah '.$keterangan,array('class'=>'btn btn-primary btn-sm'));?></h3>
                        <table id="mytable" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                          <thead>
                            <tr>
							  <th class="col-xs-1">No</th>
							  <th class="col-xs-5">Jurusan</th>
							  <th class="col-xs-3">Kelas</th>
							  <th class="col-xs-3">Aksi</th>
                            </tr>
                          </thead>
                          <tbody>	
							<?php
							if(!empty($userRecords))
							{
								$i=1;
								foreach($userRecords as $record)
								{
							?>
							<tr>
							  <td><?php echo $i++ ?></td>
							  <td><?php echo $record->jurusan ?></td>
							  <td><?php echo $record->nama_kelas ?></td>
							  <td class="text-center">
								  <a class="btn btn-sm btn-warning" title="Edit Kelas" href="<?php echo base_url().'kelas/editkelas/'.$record->id_kelas; ?>"><i class="fa fa-pencil-square-o"></i></a>
								  <button type="button" name="delete" id="<?php echo $record->id_kelas; ?>" class="btn btn-sm btn-danger delete" title="Hapus Kelas"><i class="fa fa-trash"></i></button>
							  </td>
							</tr>
							<?php
								}
							}
							?>
						  </tbody>
                        </table>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div>                
              </div><!-- /.col -->
            </div><!-- /.row -->
          </section><!-- /.content -->
        <script type="text/javascript">
			$(document).ready(function() { var table = $('#mytable').DataTable(); } );
			
			$(document).on('click', '.delete', function(){  
			   var id_kelas = $(this).attr("id");  
			   if(confirm("Apakah Anda Yakin Ingin Menghapus Kelas Ini?"))  
			   {  
					$.ajax({  
						 url:"<?php echo base_url('kelas/hapuskelas'); ?>",  
						 method:"POST",  
						 data:{id_kelas:id_kelas}  
					});
					var row = $(this).closest("tr");
					$('#mytable').DataTable().row(row).remove().draw();
			   }  
			   else  
			   {  
					return false;       
			   }  
			}); 
		</script>
</div>
