<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Tambah Kelas
        <small><?= $keterangan ?></small>
      </h1>
    </section>    
    <section class="content">    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->               
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Masukkan Data <?= $keterangan ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="tambahKelas" action="<?php echo base_url('kelas/tambahkelasbaru') ?>" method="post" role="form">
                        <div class="box-body">
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jurusan">Jurusan</label>
                                        <select class="form-control required" id="jurusan" name="jurusan">
                                            <option value="">Pilih Jurusan</option>
											<option value="Teknik Instalasi Tenaga Listrik">Teknik Instalasi Tenaga Listrik</option>
											<option value="Teknik Pemesinan">Teknik Pemesinan</option>
											<option value="Teknik Pengelasan">Teknik Pengelasan</option>
											<option value="Teknik Kendaraan Ringan">Teknik Kendaraan Ringan</option>
											<option value="Teknik Sepeda Motor">Teknik Sepeda Motor</option>
											<option value="Teknik Komputer Jaringan">Teknik Komputer Jaringan</option>
											<option value="Teknik Tata Ruang dan Pendingin (AC)">Teknik Tata Ruang dan Pendingin (AC)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="kelas">Kelas</label>
										<input type="text" class="form-control required" id="kelas" name="kelas" maxlength="32">
                                    </div>
                                </div> 
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
	<script>
	$('#jurusan').val('<?= $jurusan; ?>');
	</script>
	<script src="<?php echo base_url(); ?>assets/js/tambahKelas.js" type="text/javascript"></script>
	<section class="content">
            <div class="row">
              <div class="col-xs-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?= $keterangan ?></a></li>
                  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                        <table id="mytable" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                          <thead>
                            <tr>
							  <th class="col-xs-1">No</th>
							  <th class="col-xs-5">Jurusan</th>
							  <th class="col-xs-3">Kelas</th>
							  <th class="col-xs-3">Aksi</th>
                            </tr>
                          </thead>
                          <tbody>	
							<?php
							if(!empty($userRecords))
							{
								$i=1;
								foreach($userRecords as $record)
								{
							?>
							<tr>
							  <td><?php echo $i++ ?></td>
							  <td><?php echo $record->jurusan ?></td>
							  <td><?php echo $record->nama_kelas ?></td>
							  <td class="text-center">
								  <a class="btn btn-sm btn-warning" title="Edit Kelas" href="<?php echo base_url().'kelas/editkelas/'.$record->id_kelas; ?>"><i class="fa fa-pencil-square-o"></i></a>
								  <button type="button" name="delete" id="<?php echo $record->id_kelas; ?>" class="btn btn-sm btn-danger delete" title="Hapus Kelas"><i class="fa fa-trash"></i></button>
							  </td>
							</tr>
							<?php
								}
							}
							?>
						  </tbody>
                        </table>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div>                
              </div><!-- /.col -->
            </div><!-- /.row -->
          </section><!-- /.content -->
        <script type="text/javascript">
			$(document).ready(function() { var table = $('#mytable').DataTable(); } );
			
			$(document).on('click', '.delete', function(){  
			   var id_kelas = $(this).attr("id");  
			   if(confirm("Apakah Anda Yakin Ingin Menghapus Kelas Ini?"))  
			   {  
					$.ajax({  
						 url:"<?php echo base_url('kelas/hapuskelas'); ?>",  
						 method:"POST",  
						 data:{id_kelas:id_kelas}  
					});
					var row = $(this).closest("tr");
					$('#mytable').DataTable().row(row).remove().draw();
			   }  
			   else  
			   {  
					return false;       
			   }  
			}); 
		</script>
</div>

