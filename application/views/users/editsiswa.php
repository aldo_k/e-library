<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Tambah Pengguna
        <small><?= $keterangan ?></small>
      </h1>
    </section>    
    <section class="content">    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->               
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Masukkan Data <?= $keterangan ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="editUser" action="<?php echo base_url('user/EditSiswanya') ?>" method="post" role="form" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="no_identitas">NIS</label>
                                        <input type="text" class="form-control required no_identitas" id="no_identitas"  name="no_identitas" maxlength="18" value="<?= $no_identitas?>">
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="nama">Nama Lengkap</label>
                                        <input type="text" class="form-control required" id="nama" name="nama" maxlength="64" value="<?= $nama ?>">
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tempat_lahir">Tempat Lahir</label>
                                        <input type="text" class="form-control required" id="tempat_lahir"  name="tempat_lahir" maxlength="32" value="<?= $tempat_lahir ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="tgl_lahir">Tanggal Lahir</label>
                                        <input type="text" class="form-control required" id="tgl_lahir" name="tgl_lahir" maxlength="10" value="<?= $tgl_lahir ?>">
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="telepon">Telepon</label>
                                        <input type="text" class="form-control required digits" id="telepon" name="telepon" maxlength="14" value="<?= $telepon?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jenis_kelamin">Jenis Kelamin</label>
                                        <select class="form-control required" id="jenis_kelamin" name="jenis_kelamin">
                                            <option value="">Pilih Jenis Kelamin</option>
                                            <option value="L">Laki-Laki</option>
											<option value="P">Perempuan</option>
                                        </select>
                                    </div>
                                </div>    
                            </div>
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jurusan">Jurusan</label>
                                        <select class="form-control required" id="jurusan" name="jurusan">
                                            <option value="">Pilih Jurusan</option>
											<option value="Teknik Instalasi Tenaga Listrik">Teknik Instalasi Tenaga Listrik</option>
											<option value="Teknik Pemesinan">Teknik Pemesinan</option>
											<option value="Teknik Pengelasan">Teknik Pengelasan</option>
											<option value="Teknik Kendaraan Ringan">Teknik Kendaraan Ringan</option>
											<option value="Teknik Sepeda Motor">Teknik Sepeda Motor</option>
											<option value="Teknik Komputer Jaringan">Teknik Komputer Jaringan</option>
											<option value="Teknik Tata Ruang dan Pendingin (AC)">Teknik Tata Ruang dan Pendingin (AC)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="kelas">Kelas</label>
										<select class="form-control required" id="kelas" name="kelas">
                                            <option value="">Pilih Kelas</option>
											<?php foreach ($listkelas as $kelasnya){
													 echo '<option value="'.$kelasnya->id_kelas.'">'.$kelasnya->nama_kelas.'</option>'; 
													 }
											?>
                                        </select>
                                    </div>
                                </div> 
                            </div>
							<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="alamat">Alamat</label>
                                        <input type="text" class="form-control required" id="alamat" name="alamat" maxlength="128" value="<?= $alamat ?>">
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="foto">Foto</label>
                                        <input type="file" class="form-control required" id="foto" name="foto" accept="image/jpg, image/jpeg, image/png">
                                    </div>
                                </div> 
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="masa_berlaku">Masa Berlaku</label>
                                        <input type="number" class="form-control required" id="masa_berlaku" name="masa_berlaku" min="<?= date('Y')?>" max="<?= date('Y')+3?>" value="<?= $masa_berlaku ?>">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" class="form-control required username" id="username" name="username" maxlength="24" value="<?= $username ?>">
                                    </div>
                                </div>    
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control required" id="password"  name="password" maxlength="32">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword">Ulangi Password</label>
                                        <input type="password" class="form-control required equalTo" id="cpassword" name="cpassword" maxlength="32">
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
							<input type="hidden" value="<?= $username ?>" id="usernamehidden">
							<input type="hidden" value="<?= $no_identitas ?>" id="no_identitashidden">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    <script>
	  $(function () {
		$('#tgl_lahir').datepicker({
		  format: 'yyyy-mm-dd',
		  autoclose: true,
		});
	  });
	  $('select[name="jurusan"]').on('change', function(){
		$.ajax({
			type : 'POST', 
			url  : '<?php echo site_url('user/listkelas'); ?>', 
			data : {
				jurusan : $(this).val()
			}, 
			success : function(option){
				$('select[name="kelas"]').html(option); 
			}
		}); 
	});
	$('#jenis_kelamin').val('<?= $jenis_kelamin; ?>');
	$('#jurusan').val('<?= $jurusan; ?>');
	$('#kelas').val('<?= $kelas; ?>');
	</script>
</div>
<script src="<?php echo base_url(); ?>assets/js/editSiswa.js" type="text/javascript"></script>