<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content">
            <div class="row">
              <div class="col-xs-12">
			  <div class='box'>
                <div class='box-header'>
					<ol class="breadcrumb">
						<li><a href="<?=base_url()?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
						<li class="active"><?= $keterangan ?></li>
					</ol>
				</div>
			  </div>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><?= $keterangan ?></a></li>
                  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
					<h3><?php echo anchor('user/Tambahanggotasiswa','<i class="fa fa-plus"></i> Tambah '.$keterangan,array('class'=>'btn btn-primary btn-sm'));?></h3>
                        <table id="mytable" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                          <thead>
                            <tr>
							  <th class="col-xs-1">No</th>
							  <th class="col-xs-2">Nama Lengkap</th>
							  <th class="col-xs-1">Masa Berlaku</th>
							  <th class="col-xs-1">NIS</th>
							  <th class="col-xs-3">Jurusan</th>
							  <th class="col-xs-1">Kelas</th>
							  <th class="col-xs-1">Peminjaman Buku</th>
							  <th class="col-xs-2">Aksi</th>
                            </tr>
                          </thead>
                          <tbody>	
							<?php
							if(!empty($userRecords))
							{
								$i=1;
								foreach($userRecords as $record)
								{
							?>
							<tr>
							  <td><?php echo $i++ ?></td>
							  <td><?php echo $record->nama?><?php if($record->masa_berlaku<date('Y')){ echo " (Expired)";}?></td>
							  <td><?php echo $record->masa_berlaku ?></td>
							  <td><?php echo $record->no_identitas ?></td>
							  <td><?php echo $record->jurusan ?></td>
							  <td><?php echo $record->kelas ?></td>
							  <td><?php $jumlahbuku=$this->user_model->jumlahpinjamSiswabyID($record->no_identitas)->jumlahbuku; if(empty($jumlahbuku)){ echo "Tidak Ada"; } else { echo $jumlahbuku." Buah"; } ?></td>
							  <td class="text-center">
								  <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Lihat Detail Siswa" onclick="detail_siswa('<?php echo $record->no_identitas; ?>')"><i class="fa fa-eye"></i></a>
								  <a class="btn btn-sm btn-warning" title="Edit Siswa" href="<?php echo base_url().'user/EditSiswa/'.$record->no_identitas; ?>"><i class="fa fa-pencil-square-o"></i></a>
								  <?php if(!empty($jumlahbuku)){ ?><button type="button" class="btn btn-sm btn-danger" title="Hapus Siswa" onclick="javascript: alert('Siswa Harus Mengembalikan Buku Terlebih Dahulu');"><i class="fa fa-trash"></i></button> <?php } else { ?> <button type="button" name="delete" id="<?php echo $record->no_identitas; ?>" class="btn btn-sm btn-danger delete" title="Hapus Siswa"><i class="fa fa-trash"></i></button> <?php } ?>
							  </td>
							</tr>
							<?php
								}
							}
							?>
						  </tbody>
                        </table>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div>                
              </div><!-- /.col -->
            </div><!-- /.row -->
          </section><!-- /.content -->
        <script type="text/javascript">
			$(document).ready(function() { var table = $('#mytable').DataTable(); } );
			
			$(document).on('click', '.delete', function(){  
			   var no_identitas = $(this).attr("id");  
			   if(confirm("Apakah Anda Yakin Ingin Menghapus Siswa Ini?"))  
			   {  
					$.ajax({  
						 url:"<?php echo base_url('user/deleteSiswa'); ?>",  
						 method:"POST",  
						 data:{no_identitas:no_identitas}  
					});
					var row = $(this).closest("tr");
					$('#mytable').DataTable().row(row).remove().draw();
			   }  
			   else  
			   {  
					return false;       
			   }  
			}); 
			
			function detail_siswa(id)
			{
				save_method = 'update';
				$('#form')[0].reset();
				$('.form-group').removeClass('has-error');
				$('.help-block').empty();
			 
				$.ajax({
					url : "<?php echo site_url('user/Detailsiswa/')?>/" + id,
					type: "GET",
					dataType: "JSON",
					success: function(data)
					{
			 
						$('.no_identitas').text(data.no_identitas);
						$('.nama').text(data.nama);
						$('.tempat_lahir').text(data.tempat_lahir);
						$('.tgl_lahir').text(data.tgl_lahir);
						$('.alamat').text(data.alamat);
						$('.telepon').text(data.telepon);
						$('.username').text(data.username);
						$('.jurusan').text(data.jurusan);
						$('.kelas').text(data.kelas);
						$('.foto').attr('src', '<?= base_url('assets/foto/siswa')?>/'+data.foto);
						//$('.foto').text('<img src="assets/foto/siswa/'+data.foto+'">');
						$('.masa_berlaku').text(data.masa_berlaku);
						if((data.jenis_kelamin)='L'){ $('.jenis_kelamin').text('Laki-Laki'); }
							else if((data.jenis_kelamin)='P') { $('.jenis_kelamin').text('Perempuan'); }
							else { $('.jenis_kelamin').text('Jenis Kelamin Tidak Diketahui'); }
						
						$('#modal_form').modal('show');
						$('.modal-title').text('Detail Data Siswa');
			 
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error get data from ajax');
					}
				});
			}
        </script>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">NIS</label>
                            <div class="col-md-9 no_identitas">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Lengkap</label>
                            <div class="col-md-9 nama">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tempat Lahir</label>
                            <div class="col-md-9 tempat_lahir">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal Lahir</label>
                            <div class="col-md-9 tgl_lahir">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Telepon</label>
                            <div class="col-md-9 telepon">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Jenis Kelamin</label>
                            <div class="col-md-9 jenis_kelamin">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Jurusan</label>
                            <div class="col-md-9 jurusan">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Kelas</label>
                            <div class="col-md-9 kelas">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Alamat</label>
                            <div class="col-md-9 alamat">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Masa Berlaku</label>
                            <div class="col-md-9 masa_berlaku">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Username</label>
                            <div class="col-md-9 username">
                                
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Foto</label>
                            <div class="col-md-9">
                                <img src="" width="340px" class="foto">
                            </div>
                        </div>
						
                    </div>
					</form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-sm btn-warning" title="Edit Siswa" href="<?php echo base_url().'user/EditSiswa/'.$record->no_identitas; ?>"><i class="fa fa-pencil-square-o"></i> Edit Siswa</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

