<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class User extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->isLoggedIn();   
    }
    
    public function index()
    {
        $data['siswa']=$this->db->count_all_results('tbl_anggota');
        $data['gurustaff']=$this->db->count_all_results('tbl_guru_dan_staff');
        $data['kehilanganbuku']=$this->db->count_all_results('tbl_kehilangan_buku');
        $data['peminjamansiswa']=$this->db->where('NIP', '');
        $data['peminjamansiswa']=$this->db->count_all_results('tbl_peminjaman');
        $data['peminjamangurustaff']=$this->db->where('NIS', '');
        $data['peminjamangurustaff']=$this->db->count_all_results('tbl_peminjaman');
        $data['pengembalian']=$this->db->count_all_results('tbl_pengembalian');
        $data['buku']=$this->db->count_all_results('tbl_buku');
        $data['kategoribuku']=$this->db->count_all_results('tbl_kategori_buku');
		
		$this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Dashboard';
        $this->loadViews("dashboard", $this->global, $data , NULL);
    }
    
    function Petugas()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'Petugas Perpustakaan';
            $this->load->model('user_model');     
            $data['userRecords'] = $this->user_model->userPetugas();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Petugas';
            $this->loadViews("users/petugas", $this->global, $data, NULL);
        }
    }
	
	function Siswa()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'Anggota Siswa Perpustakaan';
            $this->load->model('user_model');     
            $data['userRecords'] = $this->user_model->userSiswa();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Petugas';
            $this->loadViews("users/siswa", $this->global, $data, NULL);
        }
    }
	
	function Guru_staff()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'Anggota Guru / Staff Perpustakaan';
            $this->load->model('user_model');     
            $data['userRecords'] = $this->user_model->userGuruStaff();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Petugas';
            $this->loadViews("users/guru_staff", $this->global, $data, NULL);
        }
    }

	public function Detailpetugas($no_identitas)
    {
        $data = $this->user_model->getDetailInfoPetugas($no_identitas);
        echo json_encode($data);
    }
	
	public function Detailgurustaff($no_identitas)
    {
        $data = $this->user_model->getDetailInfoGuruStaff($no_identitas);
        echo json_encode($data);
    }
	
	public function Detailsiswa($no_identitas)
    {
        $data = $this->user_model->getDetailInfoSiswa($no_identitas);
        echo json_encode($data);
    }
   
   function Tambahpetugas()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            $data['keterangan'] = 'Petugas Perpustakaan';
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Tambah Petugas Baru';

            $this->loadViews("users/tambahpetugas", $this->global, $data, NULL);
        }
    }
	
	function tambahpetugasbaru()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
			$this->form_validation->set_rules('no_identitas', 'no identitas', 'trim|required|xss_clean');
			$this->form_validation->set_rules('nama', 'nama', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tgl_lahir', 'tgl lahir', 'trim|required|xss_clean');
			$this->form_validation->set_rules('telepon', 'telepon', 'trim|required||min_length[10]|xss_clean');
			$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jabatan', 'jabatan', 'trim|required|xss_clean');
			$this->form_validation->set_rules('alamat', 'alamat', 'trim|required|xss_clean');
			$this->form_validation->set_rules('username', 'username', 'trim');
            $this->form_validation->set_rules('password','Password','required|max_length[20]|xss_clean');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]|xss_clean');
			//$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			// baru bates sini ... rest dulu metak palak :D            
            if($this->form_validation->run() == FALSE)
            {
                $this->Tambahpetugas();
            }
            else
            {
                $no_identitas = $this->input->post('no_identitas');
                $nama = ucwords(strtolower($this->input->post('nama')));
                $tempat_lahir = $this->input->post('tempat_lahir');
				$tgl_lahir = $this->input->post('tgl_lahir');
				$telepon = $this->input->post('telepon');
				$jenis_kelamin = $this->input->post('jenis_kelamin');
				$jabatan = $this->input->post('jabatan');
				$foto = $this->input->post('foto');
				$alamat = $this->input->post('alamat');
                $username = $this->input->post('username');
                $password = $this->input->post('password');
				
				$akunPenggunaBaru = array('username'=>$username, 'password'=>getHashedPassword($password), 'usertype'=>'1', 
                                    'no_identitas'=>$no_identitas);
				$dataLengkapPengguna = array('id_petugas'=>$no_identitas, 'nama'=>$nama, 'tempat_lahir'=> $tempat_lahir,'tgl_lahir'=>$tgl_lahir, 'alamat'=>$alamat, 'telepon'=>$telepon, 'jenis_kelamin'=>$jenis_kelamin, 'jabatan'=>$jabatan);
                if($_FILES['foto']['name'] !='') {
				$file_name = $_FILES['foto']['name'];
				$file_name_rename = $this->input->post('no_identitas').date('YmdHis');
			    $explode = explode('.', $file_name);
			    if(count($explode) >= 2) {
		            $new_file = $file_name_rename.'.'.$explode[1];
					$config['upload_path'] = './assets/foto/petugas';
					$config['allowed_types'] = 'jpg|jpeg|png';
					$config['file_name'] = $new_file;
					$config['max_size'] = '10240';
					$data['foto'] = $new_file;
					$fieldname='foto';
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if($this->upload->do_upload($fieldname)) {
						$data_upload = $this->upload->data();
						$dataLengkapPengguna["foto"] = $new_file;
						$this->load->model('user_model');
						$result1 = $this->user_model->TambahPenggunaBaru($akunPenggunaBaru);
						$result2 = $this->user_model->TambahPetugasBaru($dataLengkapPengguna);
						$this->session->set_flashdata('success', 'Pengguna Baru Berhasil Ditambahkan');
						redirect(site_url('user/tambahpetugas'));
					} else {
						$this->Tambahpetugas();
						echo "<script type=\"text/javascript\">alert('Foto Yg Dimasukkan Harus Berformat *.jpg / *.png');</script>";
					}
				} else {
					$this->Tambahpetugas();
					echo "<script type=\"text/javascript\">alert('Foto Yg Anda Masukkan Tidak Diizinkan');</script>";
					}
				}			                 
            }
        }
    }

	function Tambahgurustaff()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            $data['keterangan'] = 'Guru / Staff Anggota Perpustakaan';
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Tambah Guru / Staff Anggota Baru';

            $this->loadViews("users/tambahgurustaff", $this->global, $data, NULL);
        }
    }
	
	function tambahgurustaffbaru()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
			$this->form_validation->set_rules('no_identitas', 'no identitas', 'trim|required|xss_clean');
			$this->form_validation->set_rules('nama', 'nama', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tgl_lahir', 'tgl lahir', 'trim|required|xss_clean');
			$this->form_validation->set_rules('telepon', 'telepon', 'trim|required||min_length[10]|xss_clean');
			$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jabatan', 'jabatan', 'trim|required|xss_clean');
			$this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
			$this->form_validation->set_rules('alamat', 'alamat', 'trim|required|xss_clean');
			$this->form_validation->set_rules('username', 'username', 'trim');
            $this->form_validation->set_rules('password','Password','required|max_length[20]|xss_clean');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]|xss_clean');
			//$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
            if($this->form_validation->run() == FALSE)
            {
                $this->Tambahgurustaff();
            }
            else
            {
                $no_identitas = $this->input->post('no_identitas');
                $nama = ucwords(strtolower($this->input->post('nama')));
                $tempat_lahir = $this->input->post('tempat_lahir');
				$tgl_lahir = $this->input->post('tgl_lahir');
				$telepon = $this->input->post('telepon');
				$jenis_kelamin = $this->input->post('jenis_kelamin');
				$jabatan = $this->input->post('jabatan');
				$status = $this->input->post('status');
				$foto = $this->input->post('foto');
				$alamat = $this->input->post('alamat');
                $username = $this->input->post('username');
                $password = $this->input->post('password');
				
				$akunPenggunaBaru = array('username'=>$username, 'password'=>getHashedPassword($password), 'usertype'=>'2', 'no_identitas'=>$no_identitas);
				$dataLengkapPengguna = array('NIP'=>$no_identitas, 'nama'=>$nama, 'tempat_lahir'=> $tempat_lahir,'tgl_lahir'=>$tgl_lahir, 'alamat'=>$alamat, 'telepon'=>$telepon, 'jenis_kelamin'=>$jenis_kelamin, 'jabatan'=>$jabatan, 'status'=>$status);
                if($_FILES['foto']['name'] !='') {
				$file_name = $_FILES['foto']['name'];
				$file_name_rename = $this->input->post('no_identitas').date('YmdHis');
			    $explode = explode('.', $file_name);
			    if(count($explode) >= 2) {
		            $new_file = $file_name_rename.'.'.$explode[1];
					$config['upload_path'] = './assets/foto/gurustaff';
					$config['allowed_types'] = 'jpg|jpeg|png';
					$config['file_name'] = $new_file;
					$config['max_size'] = '10240';
					$data['foto'] = $new_file;
					$fieldname='foto';
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if($this->upload->do_upload($fieldname)) {
						$data_upload = $this->upload->data();
						$dataLengkapPengguna["foto"] = $new_file;
						$this->load->model('user_model');
						$result1 = $this->user_model->TambahPenggunaBaru($akunPenggunaBaru);
						$result2 = $this->user_model->TambahGuruStaffBaru($dataLengkapPengguna);
						$this->session->set_flashdata('success', 'Pengguna Baru Berhasil Ditambahkan');

						redirect(site_url('user/tambahgurustaff'));
					} else {
						$this->Tambahgurustaff();
						echo "<script type=\"text/javascript\">alert('Foto Yg Dimasukkan Harus Berformat *.jpg / *.png');</script>";
					}
				} else {
					$this->Tambahgurustaff();
					echo "<script type=\"text/javascript\">alert('Foto Yg Anda Masukkan Tidak Diizinkan');</script>";
					}
				}			                 
            }
        }
    }
	
	function Tambahanggotasiswa()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            $data['keterangan'] = 'Anggota Siswa Perpustakaan';
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Tambah Anggota Siswa Baru';

            $this->loadViews("users/tambahanggotasiswa", $this->global, $data, NULL);
        }
    }
	
	function tambahanggotasiswabaru()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
			$this->form_validation->set_rules('no_identitas', 'no identitas', 'trim|required|xss_clean');
			$this->form_validation->set_rules('nama', 'nama', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tgl_lahir', 'tgl lahir', 'trim|required|xss_clean');
			$this->form_validation->set_rules('telepon', 'telepon', 'trim|required||min_length[10]|xss_clean');
			$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jurusan', 'jurusan', 'trim|required|xss_clean');
			$this->form_validation->set_rules('kelas', 'kelas', 'trim|required|xss_clean');
			$this->form_validation->set_rules('alamat', 'alamat', 'trim|required|xss_clean');
			$this->form_validation->set_rules('masa_berlaku', 'masa berlaku', 'trim|required|xss_clean');
			$this->form_validation->set_rules('username', 'username', 'trim');
            $this->form_validation->set_rules('password','Password','required|max_length[20]|xss_clean');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]|xss_clean');
			//$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
			// baru bates sini ... rest dulu metak palak :D            
            if($this->form_validation->run() == FALSE)
            {
                $this->Tambahanggotasiswa();
            }
            else
            {
                $no_identitas = $this->input->post('no_identitas');
                $nama = ucwords(strtolower($this->input->post('nama')));
                $tempat_lahir = $this->input->post('tempat_lahir');
				$tgl_lahir = $this->input->post('tgl_lahir');
				$telepon = $this->input->post('telepon');
				$jenis_kelamin = $this->input->post('jenis_kelamin');
				$jurusan = $this->input->post('jurusan');
				$kelas = $this->input->post('kelas');
				$foto = $this->input->post('foto');
				$masa_berlaku = $this->input->post('masa_berlaku');
				$alamat = $this->input->post('alamat');
                $username = $this->input->post('username');
                $password = $this->input->post('password');
				
				$akunPenggunaBaru = array('username'=>$username, 'password'=>getHashedPassword($password), 'usertype'=>'3', 'no_identitas'=>$no_identitas);
				$dataLengkapPengguna = array('NIS'=>$no_identitas, 'nama'=>$nama, 'tempat_lahir'=> $tempat_lahir,'tgl_lahir'=>$tgl_lahir, 'alamat'=>$alamat, 'telepon'=>$telepon, 'jenis_kelamin'=>$jenis_kelamin, 'id_kelas'=>$kelas, 'masa_berlaku'=>$masa_berlaku);
                if($_FILES['foto']['name'] !='') {
				$file_name = $_FILES['foto']['name'];
				$file_name_rename = $this->input->post('no_identitas').date('YmdHis');
			    $explode = explode('.', $file_name);
			    if(count($explode) >= 2) {
		            $new_file = $file_name_rename.'.'.$explode[1];
					$config['upload_path'] = './assets/foto/siswa';
					$config['allowed_types'] = 'jpg|jpeg|png';
					$config['file_name'] = $new_file;
					$config['max_size'] = '10240';
					$data['foto'] = $new_file;
					$fieldname='foto';
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if($this->upload->do_upload($fieldname)) {
						$data_upload = $this->upload->data();
						$dataLengkapPengguna["foto"] = $new_file;
						$this->load->model('user_model');
						$result1 = $this->user_model->TambahPenggunaBaru($akunPenggunaBaru);
						$result2 = $this->user_model->TambahAnggotaSiswaBaru($dataLengkapPengguna);
						$this->session->set_flashdata('success', 'Pengguna Baru Berhasil Ditambahkan');
						

						redirect(site_url('user/Tambahanggotasiswa'));
					} else {
						$this->Tambahanggotasiswa();
						echo "<script type=\"text/javascript\">alert('Foto Yg Dimasukkan Harus Berformat *.jpg / *.png');</script>";
					}
				} else {
					$this->Tambahanggotasiswa();
					echo "<script type=\"text/javascript\">alert('Foto Yg Anda Masukkan Tidak Diizinkan');</script>";
					}
				}			                 
            }
        }
    }

    function cekUsername()
    {
        $no_identitas = $this->input->post("no_identitas");
        $username = $this->input->post("username");

        if(empty($no_identitas)){
            $result = $this->user_model->cekUsername($username);
        } else {
            $result = $this->user_model->cekUsername($username, $no_identitas);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
	
	function cekNoIdentitas()
    {
        $no_identitas = $this->input->post("no_identitas");
        $username = $this->input->post("username");

        if(empty($username)){
            $result = $this->user_model->cekNoIdentitas($no_identitas);
        } else {
            $result = $this->user_model->cekNoIdentitas($no_identitas, $username);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
    
	public function listkelas()
	{
	 $kelas = $this->user_model->listkelas($this->input->post('jurusan')); 
	 
	 echo '<option> Pilih Kelas </option>';
	 foreach ($kelas as $kelasnya){
	 echo '<option value="'.$kelasnya->id_kelas.'">'.$kelasnya->nama_kelas.'</option>'; 
	 }
	 }
	
    function EditPetugas($no_identitas = NULL)
    {
        if($this->isAdmin() == TRUE || $no_identitas == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($no_identitas == null)
            {
                redirect('user/petugas');
            }
            
            $row = $this->user_model->getDetailInfoPetugas($no_identitas);
			$data = array(
			'no_identitas' => $row->no_identitas,
			'nama' => $row->nama,
			'tempat_lahir' => $row->tempat_lahir,
			'tgl_lahir' => $row->tgl_lahir,
			'alamat' => $row->alamat,
			'telepon' => $row->telepon,
			'jenis_kelamin' => $row->jenis_kelamin,
			'foto' => $row->foto,
			'jabatan' => $row->jabatan,
			'username' => $row->username,
			);
            $data['keterangan'] = 'Petugas Perpustakaan';
			
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Edit Petugas';
            
            $this->loadViews("users/editpetugas", $this->global, $data, NULL);
        }
    }
	   
    function Editpetugasnya()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
			$this->form_validation->set_rules('no_identitas', 'no identitas', 'trim|required|xss_clean');
			$this->form_validation->set_rules('nama', 'nama', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tgl_lahir', 'tgl lahir', 'trim|required|xss_clean');
			$this->form_validation->set_rules('telepon', 'telepon', 'trim|required||min_length[10]|xss_clean');
			$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jabatan', 'jabatan', 'trim|required|xss_clean');
			$this->form_validation->set_rules('alamat', 'alamat', 'trim|required|xss_clean');
			$this->form_validation->set_rules('username', 'username', 'trim');
            $this->form_validation->set_rules('password','Password','max_length[20]|xss_clean');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|matches[password]|max_length[20]|xss_clean');
			//$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');          
            if($this->form_validation->run() == FALSE)
            {
                $this->EditPetugas($no_identitas);
            }
            else
            {
				$row = $this->user_model->getDetailInfoPetugas($this->input->post('no_identitas'));
                $no_identitas = $this->input->post('no_identitas');
                $nama = ucwords(strtolower($this->input->post('nama')));
                $tempat_lahir = $this->input->post('tempat_lahir');
				$tgl_lahir = $this->input->post('tgl_lahir');
				$telepon = $this->input->post('telepon');
				$jenis_kelamin = $this->input->post('jenis_kelamin');
				$jabatan = $this->input->post('jabatan');
				$foto = $this->input->post('foto');
				$alamat = $this->input->post('alamat');
                $username = $this->input->post('username');
                $password = $this->input->post('password');
				if (empty($password)){
				$akunPenggunaBaru = array('username'=>$username, 'no_identitas'=>$no_identitas); 
				} else {
				$akunPenggunaBaru = array('username'=>$username, 'password'=>getHashedPassword($password), 'no_identitas'=>$no_identitas); 	
				}
				$dataLengkapPengguna = array('id_petugas'=>$no_identitas, 'nama'=>$nama, 'tempat_lahir'=> $tempat_lahir,'tgl_lahir'=>$tgl_lahir, 'alamat'=>$alamat, 'telepon'=>$telepon, 'jenis_kelamin'=>$jenis_kelamin, 'jabatan'=>$jabatan);
                if($_FILES['foto']['name'] !='') {
				$file_name = $_FILES['foto']['name'];
				$file_name_rename = $this->input->post('no_identitas').date('YmdHis');
			    $explode = explode('.', $file_name);
			    if(count($explode) >= 2) {
		            $new_file = $file_name_rename.'.'.$explode[1];
					$config['upload_path'] = './assets/foto/petugas';
					$config['allowed_types'] = 'jpg|jpeg|png';
					$config['file_name'] = $new_file;
					$config['max_size'] = '10240';
					$data['foto'] = $new_file;
					$fieldname='foto';
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if($this->upload->do_upload($fieldname)) {
						$data_upload = $this->upload->data();
						$dataLengkapPengguna["foto"] = $new_file;
						unlink(FCPATH.'assets/foto/petugas/'.$row->foto);
						$this->load->model('user_model');
						$result1 = $this->user_model->editPengguna($akunPenggunaBaru , $no_identitas );
						$result2 = $this->user_model->editPetugas($dataLengkapPengguna , $no_identitas );
						$this->session->set_flashdata('success', 'Pengguna Baru Berhasil Ditambahkan');

						redirect(site_url('user/petugas'));
					} else {
						$this->EditPetugas($no_identitas);
						echo "<script type=\"text/javascript\">alert('Foto Yg Dimasukkan Harus Berformat *.jpg / *.png');</script>";
					}
				} else {
					$this->EditPetugas($no_identitas);
					echo "<script type=\"text/javascript\">alert('Foto Yg Anda Masukkan Tidak Diizinkan');</script>";
					}
				}
					$result1 = $this->user_model->editPengguna($akunPenggunaBaru , $no_identitas );
					$result2 = $this->user_model->editPetugas($dataLengkapPengguna , $no_identitas );
					$this->session->set_flashdata('success', 'Edit Data Berhasil');
					redirect(site_url('user/petugas'));
            }
        }
    }
	
	function EditGuruStaff($no_identitas = NULL)
    {
        if($this->isAdmin() == TRUE || $no_identitas == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($no_identitas == null)
            {
                redirect('user/guru_staff');
            }
            
            $row = $this->user_model->getDetailInfoGuruStaff($no_identitas);
			$data = array(
			'no_identitas' => $row->no_identitas,
			'nama' => $row->nama,
			'tempat_lahir' => $row->tempat_lahir,
			'tgl_lahir' => $row->tgl_lahir,
			'alamat' => $row->alamat,
			'telepon' => $row->telepon,
			'jenis_kelamin' => $row->jenis_kelamin,
			'foto' => $row->foto,
			'jabatan' => $row->jabatan,
			'status' => $row->status,
			'username' => $row->username,
			);
            $data['keterangan'] = 'Anggota Guru / Staff Perpustakaan';
			
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Edit Guru / Staff';
            
            $this->loadViews("users/editgurustaff", $this->global, $data, NULL);
        }
    }
	
    function EditGuruStaffnya()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
			$this->form_validation->set_rules('no_identitas', 'no identitas', 'trim|required|xss_clean');
			$this->form_validation->set_rules('nama', 'nama', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tgl_lahir', 'tgl lahir', 'trim|required|xss_clean');
			$this->form_validation->set_rules('telepon', 'telepon', 'trim|required||min_length[10]|xss_clean');
			$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jabatan', 'jabatan', 'trim|required|xss_clean');
			$this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
			$this->form_validation->set_rules('alamat', 'alamat', 'trim|required|xss_clean');
			$this->form_validation->set_rules('username', 'username', 'trim');
            $this->form_validation->set_rules('password','Password','max_length[20]|xss_clean');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|matches[password]|max_length[20]|xss_clean');
			//$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');          
            if($this->form_validation->run() == FALSE)
            {
                $this->EditGuruStaff($no_identitas);
            }
            else
            {
				$row = $this->user_model->getDetailInfoGuruStaff($this->input->post('no_identitas'));
                $no_identitas = $this->input->post('no_identitas');
                $nama = ucwords(strtolower($this->input->post('nama')));
                $tempat_lahir = $this->input->post('tempat_lahir');
				$tgl_lahir = $this->input->post('tgl_lahir');
				$telepon = $this->input->post('telepon');
				$jenis_kelamin = $this->input->post('jenis_kelamin');
				$jabatan = $this->input->post('jabatan');
				$status = $this->input->post('status');
				$foto = $this->input->post('foto');
				$alamat = $this->input->post('alamat');
                $username = $this->input->post('username');
                $password = $this->input->post('password');
				if (empty($password)){
				$akunPenggunaBaru = array('username'=>$username, 'no_identitas'=>$no_identitas); 
				} else {
				$akunPenggunaBaru = array('username'=>$username, 'password'=>getHashedPassword($password), 'no_identitas'=>$no_identitas); 	
				}
				$dataLengkapPengguna = array('NIP'=>$no_identitas, 'nama'=>$nama, 'tempat_lahir'=> $tempat_lahir,'tgl_lahir'=>$tgl_lahir, 'alamat'=>$alamat, 'telepon'=>$telepon, 'jenis_kelamin'=>$jenis_kelamin, 'jabatan'=>$jabatan,'status'=>$status);
                if($_FILES['foto']['name'] !='') {
				$file_name = $_FILES['foto']['name'];
				$file_name_rename = $this->input->post('no_identitas').date('YmdHis');
			    $explode = explode('.', $file_name);
			    if(count($explode) >= 2) {
		            $new_file = $file_name_rename.'.'.$explode[1];
					$config['upload_path'] = './assets/foto/gurustaff';
					$config['allowed_types'] = 'jpg|jpeg|png';
					$config['file_name'] = $new_file;
					$config['max_size'] = '10240';
					$data['foto'] = $new_file;
					$fieldname='foto';
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if($this->upload->do_upload($fieldname)) {
						$data_upload = $this->upload->data();
						$dataLengkapPengguna["foto"] = $new_file;
						unlink(FCPATH.'assets/foto/gurustaff/'.$row->foto);
						$this->load->model('user_model');
						$result1 = $this->user_model->editPengguna($akunPenggunaBaru , $no_identitas );
						$result2 = $this->user_model->editGuruStaff($dataLengkapPengguna , $no_identitas );
						$this->session->set_flashdata('success', 'Pengguna Baru Berhasil Ditambahkan');

						redirect(site_url('user/guru_staff'));
					} else {
						$this->EditGuruStaff($no_identitas);
						echo "<script type=\"text/javascript\">alert('Foto Yg Dimasukkan Harus Berformat *.jpg / *.png');</script>";
					}
				} else {
					$this->EditGuruStaff($no_identitas);
					echo "<script type=\"text/javascript\">alert('Foto Yg Anda Masukkan Tidak Diizinkan');</script>";
					}
				}
					$result1 = $this->user_model->editPengguna($akunPenggunaBaru , $no_identitas );
					$result2 = $this->user_model->editGuruStaff($dataLengkapPengguna , $no_identitas );
					$this->session->set_flashdata('success', 'Edit Data Berhasil');
					redirect(site_url('user/guru_staff'));
            }
        }
    }
	
	function EditSiswa($no_identitas = NULL)
    {
        if($this->isAdmin() == TRUE || $no_identitas == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($no_identitas == null)
            {
                redirect('user/siswa');
            }
            $row = $this->user_model->getDetailInfoSiswa($no_identitas);
			$data = array(
			'no_identitas' => $row->no_identitas,
			'nama' => $row->nama,
			'tempat_lahir' => $row->tempat_lahir,
			'tgl_lahir' => $row->tgl_lahir,
			'alamat' => $row->alamat,
			'telepon' => $row->telepon,
			'jenis_kelamin' => $row->jenis_kelamin,
			'foto' => $row->foto,
			'jurusan' => $row->jurusan,
			'kelas' => $row->id_kelas,
			'masa_berlaku' => $row->masa_berlaku,
			'username' => $row->username,
			);
            $data['listkelas'] = $this->user_model->listkelas($row->jurusan); 
            $data['keterangan'] = 'Siswa Perpustakaan';
			
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Edit Siswa';
            
            $this->loadViews("users/editsiswa", $this->global, $data, NULL);
        }
    }
	
    function Editsiswanya()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
			$this->form_validation->set_rules('no_identitas', 'no identitas', 'trim|required|xss_clean');
			$this->form_validation->set_rules('nama', 'nama', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tgl_lahir', 'tgl lahir', 'trim|required|xss_clean');
			$this->form_validation->set_rules('telepon', 'telepon', 'trim|required||min_length[10]|xss_clean');
			$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required|xss_clean');
			$this->form_validation->set_rules('masa_berlaku', 'masa berlaku', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jurusan', 'jurusan', 'trim|required|xss_clean');
			$this->form_validation->set_rules('kelas', 'kelas', 'trim|required|xss_clean');
			$this->form_validation->set_rules('alamat', 'alamat', 'trim|required|xss_clean');
			$this->form_validation->set_rules('username', 'username', 'trim');
            $this->form_validation->set_rules('password','Password','max_length[20]|xss_clean');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|matches[password]|max_length[20]|xss_clean');
			//$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');          
            if($this->form_validation->run() == FALSE)
            {
                $this->EditSiswa($no_identitas);
            }
            else
            {
				$row = $this->user_model->getDetailInfoSiswa($this->input->post('no_identitas'));
                $no_identitas = $this->input->post('no_identitas');
                $nama = ucwords(strtolower($this->input->post('nama')));
                $tempat_lahir = $this->input->post('tempat_lahir');
				$tgl_lahir = $this->input->post('tgl_lahir');
				$telepon = $this->input->post('telepon');
				$jenis_kelamin = $this->input->post('jenis_kelamin');
				$jurusan = $this->input->post('jurusan');
				$kelas = $this->input->post('kelas');
				$foto = $this->input->post('foto');
				$masa_berlaku = $this->input->post('masa_berlaku');
				$alamat = $this->input->post('alamat');
                $username = $this->input->post('username');
                $password = $this->input->post('password');
				if (empty($password)){
				$akunPenggunaBaru = array('username'=>$username, 'no_identitas'=>$no_identitas); 
				} else {
				$akunPenggunaBaru = array('username'=>$username, 'password'=>getHashedPassword($password), 'no_identitas'=>$no_identitas); 	
				}
				$dataLengkapPengguna = array('NIS'=>$no_identitas, 'nama'=>$nama, 'tempat_lahir'=> $tempat_lahir,'tgl_lahir'=>$tgl_lahir, 'alamat'=>$alamat, 'telepon'=>$telepon, 'jenis_kelamin'=>$jenis_kelamin, 'id_kelas'=>$kelas, 'masa_berlaku'=>$masa_berlaku);
                if($_FILES['foto']['name'] !='') {
				$file_name = $_FILES['foto']['name'];
				$file_name_rename = $this->input->post('no_identitas').date('YmdHis');
			    $explode = explode('.', $file_name);
			    if(count($explode) >= 2) {
		            $new_file = $file_name_rename.'.'.$explode[1];
					$config['upload_path'] = './assets/foto/siswa';
					$config['allowed_types'] = 'jpg|jpeg|png';
					$config['file_name'] = $new_file;
					$config['max_size'] = '10240';
					$data['foto'] = $new_file;
					$fieldname='foto';
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if($this->upload->do_upload($fieldname)) {
						$data_upload = $this->upload->data();
						$dataLengkapPengguna["foto"] = $new_file;
						unlink(FCPATH.'assets/foto/siswa/'.$row->foto);
						$this->load->model('user_model');
						$result1 = $this->user_model->editPengguna($akunPenggunaBaru , $no_identitas );
						$result2 = $this->user_model->editAnggotaSiswa($dataLengkapPengguna , $no_identitas );
						$this->session->set_flashdata('success', 'Pengguna Baru Berhasil Ditambahkan');

						redirect(site_url('user/siswa'));
					} else {
						$this->EditSiswa($no_identitas);
						echo "<script type=\"text/javascript\">alert('Foto Yg Dimasukkan Harus Berformat *.jpg / *.png');</script>";
					}
				} else {
					$this->EditSiswa($no_identitas);
					echo "<script type=\"text/javascript\">alert('Foto Yg Anda Masukkan Tidak Diizinkan');</script>";
					}
				}
					$result1 = $this->user_model->editPengguna($akunPenggunaBaru , $no_identitas );
					$result2 = $this->user_model->editAnggotaSiswa($dataLengkapPengguna , $no_identitas );
					$this->session->set_flashdata('success', 'Edit Data Berhasil');
					redirect(site_url('user/siswa'));
            }
        }
    }

    function deletePetugas()
    {
        if($this->isAdmin() == TRUE)
        {
            echo "<script type=\"text/javascript\">alert('Anda Tidak Memiliki Hak Untuk Melakukan Aksi Ini');</script>";
        }
        else
        {
            $no_identitas = $this->input->post('no_identitas');
            $row = $this->user_model->getDetailInfoPetugas($no_identitas);
            $result1 = $this->user_model->deleteAkun($no_identitas);
            $result2 = $this->user_model->deletePetugas($no_identitas); 
			unlink(FCPATH.'assets/foto/petugas/'.$row->foto);
        }
    }
	
	function deleteGuruStaff()
    {
        if($this->isAdmin() == TRUE)
        {
            echo "<script type=\"text/javascript\">alert('Anda Tidak Memiliki Hak Untuk Melakukan Aksi Ini');</script>";
        }
        else
        {
            $no_identitas = $this->input->post('no_identitas');
            $row = $this->user_model->getDetailInfoPetugas($no_identitas);
            $result1 = $this->user_model->deleteAkun($no_identitas);
            $result2 = $this->user_model->deleteGuruStaff($no_identitas); 
			unlink(FCPATH.'assets/foto/gurustaff/'.$row->foto);
        }
    }
	
	function deleteSiswa()
    {
        if($this->isAdmin() == TRUE)
        {
            echo "<script type=\"text/javascript\">alert('Anda Tidak Memiliki Hak Untuk Melakukan Aksi Ini');</script>";
        }
        else
        {
            $no_identitas = $this->input->post('no_identitas');
            $row = $this->user_model->getDetailInfoPetugas($no_identitas);
            $result1 = $this->user_model->deleteAkun($no_identitas);
            $result2 = $this->user_model->deleteSiswa($no_identitas); 
			unlink(FCPATH.'assets/foto/siswa/'.$row->foto);
        }
    }
    
    function loadChangePass()
    {
        $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Change Password';
        
        $this->loadViews("changePassword", $this->global, NULL, NULL);
    }
    
    function changePassword()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');
        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->loadChangePass();
        }
        else
        {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            
            $resultPas = $this->user_model->matchOldPassword($this->no_identitas, $oldPassword);
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Password Lama Tidak Sama');
                redirect('loadChangePass');
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword));
                
                $result = $this->user_model->changePassword($this->no_identitas, $usersData);
                
                if($result > 0) { $this->session->set_flashdata('success', 'Kata Sandi Berhasil Diperbarui'); }
                else { $this->session->set_flashdata('error', 'Gagal Memperbarui Kata Sandi'); }
                
                redirect('loadChangePass');
            }
        }
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>