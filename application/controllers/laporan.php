<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Laporan extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('kelas_model');
		$this->load->model('buku_model');
		$this->load->model('user_model');
		$this->load->model('kategori_buku_model');
		$this->load->model('peminjaman_model');
		$this->load->model('pengembalian_model');
		$this->load->model('kehilangan_model');
		$this->load->helper('exportpdf_helper');
        $this->isLoggedIn();   
    }
    
    public function index()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'Laporan Perpustakaan';
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Laporan Perpustakaan';
            $this->loadViews("laporan/index", $this->global, $data, NULL);
        }
    }
	
	public function siswa($download_pdf=TRUE)
	{
		if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
		else
        {
			$data['keterangan'] = 'Anggota Siswa Perpustakaan';
            $this->load->model('user_model');     
            $data['userRecords'] = $this->user_model->userSiswa();
			$pdf_filename = 'Laporan-Anggota-Siswa-Perpustakaan-SMK-YP-Gajah-Mada-'.date('Y-m-d-H-i-s').'.pdf';

			$user_info = $this->load->view('laporan/siswa', $data, true);
			
			$output = $user_info;
			
			if($download_pdf == TRUE)
			generate_pdf($output, $pdf_filename);
		}
	}
	
	public function gurustaff($download_pdf=TRUE)
	{
		if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
		else
        {
			$data['keterangan'] = 'Anggota Guru/Staff Perpustakaan';
            $this->load->model('user_model');     
            $data['userRecords'] = $this->user_model->userGuruStaff();
			$pdf_filename = 'Laporan-Anggota-Guru-Staff-Perpustakaan-SMK-YP-Gajah-Mada-'.date('Y-m-d-H-i-s').'.pdf';

			$user_info = $this->load->view('laporan/gurustaff', $data, true);
			
			$output = $user_info;
			
			if($download_pdf == TRUE)
			generate_pdf($output, $pdf_filename);
		}
	}
	
	public function buku($download_pdf=TRUE)
	{
		if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
		else
        {
			$data['keterangan'] = 'Laporan Data Buku Perpustakaan';
            $this->load->model('buku_model');     
			$data['bukuRecords'] = $this->buku_model->listbuku();
			$pdf_filename = 'Laporan-Data-Buku-Perpustakaan-SMK-YP-Gajah-Mada-'.date('Y-m-d-H-i-s').'.pdf';

			$user_info = $this->load->view('laporan/buku', $data, true);
			
			$output = $user_info;
			
			if($download_pdf == TRUE)
			generate_pdf($output, $pdf_filename);
		}
	}
	
	public function kehilanganbuku($download_pdf=TRUE)
	{
		if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
		else
        {
			$data['keterangan'] = 'Laporan Kehilangan Buku Perpustakaan';
            $this->load->model('kehilangan_model');     
			$data['bukuRecords'] = $this->kehilangan_model->listkehilanganbuku();
			$pdf_filename = 'Laporan-Kehilangan-Buku-Perpustakaan-SMK-YP-Gajah-Mada-'.date('Y-m-d-H-i-s').'.pdf';

			$user_info = $this->load->view('laporan/kehilanganbuku', $data, true);
			
			$output = $user_info;
			
			if($download_pdf == TRUE)
			generate_pdf($output, $pdf_filename);
		}
	}
	
	public function peminjamansiswa($download_pdf=TRUE)
	{
		if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
		else
        {
			$data['keterangan'] = 'Peminjaman Buku Siswa Perpustakaan';
			$this->load->model('peminjaman_model');     
            $data['peminjaman'] = $this->peminjaman_model->get_all_siswa();
			$pdf_filename = 'Laporan-Peminjaman-Buku-Siswa-Perpustakaan-SMK-YP-Gajah-Mada-'.date('Y-m-d-H-i-s').'.pdf';

			$user_info = $this->load->view('laporan/peminjamansiswa', $data, true);
			
			$output = $user_info;
			
			if($download_pdf == TRUE)
			generate_pdf($output, $pdf_filename);
		}
	}
	
	public function peminjamangurustaff($download_pdf=TRUE)
	{
		if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
		else
        {
			$data['keterangan'] = 'Anggota Siswa Perpustakaan';
            $this->load->model('peminjaman_model');     
            $data['peminjaman'] = $this->peminjaman_model->get_all_gurustaff();
			$pdf_filename = 'Laporan-Peminjaman-Buku-Guru-Staff-Perpustakaan-SMK-YP-Gajah-Mada-'.date('Y-m-d-H-i-s').'.pdf';

			$user_info = $this->load->view('laporan/peminjamangurustaff', $data, true);
			
			$output = $user_info;
			
			if($download_pdf == TRUE)
			generate_pdf($output, $pdf_filename);
		}
	}
	
	public function pengembalian($download_pdf=TRUE)
	{
		if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
		else
        {
			$data['keterangan'] = 'Anggota Siswa Perpustakaan';
            $this->load->model('pengembalian_model');     
            $data['pengembalian'] = $this->pengembalian_model->get_all();
			$pdf_filename = 'Laporan-Pengembalian-Buku-Perpustakaan-SMK-YP-Gajah-Mada-'.date('Y-m-d-H-i-s').'.pdf';

			$user_info = $this->load->view('laporan/pengembalian', $data, true);
			
			$output = $user_info;
			
			if($download_pdf == TRUE)
			generate_pdf($output, $pdf_filename);
		}
	}
    
}
?>