<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Kelas extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('kelas_model');
        $this->isLoggedIn();   
    }
    
    public function index()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'List Kelas';
            $this->load->model('kelas_model');     
            $data['userRecords'] = $this->kelas_model->listkelas();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Kelas';
            $this->loadViews("kelas/listkelas", $this->global, $data, NULL);
        }
    }
	
	public function tambahkelas()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'List Kelas';
            $this->load->model('kelas_model');     
            $data['userRecords'] = $this->kelas_model->listkelas();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Tambah Kelas';
            $this->loadViews("kelas/tambahkelas", $this->global, $data, NULL);
        }
    }
	
    public function tambahkelasbaru()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$this->load->library('form_validation');
            
			$this->form_validation->set_rules('jurusan', 'jurusan', 'trim|required|xss_clean');
			$this->form_validation->set_rules('kelas', 'kelas', 'trim|required|cekkelas|xss_clean');            
            if($this->form_validation->run() == FALSE)
            {
                $this->tambahkelas();
            }
            else
            {
				$jurusan = $this->input->post('jurusan');
				$kelas = $this->input->post('kelas');
				$datakelas = array('jurusan'=>$jurusan,'nama_kelas'=> $kelas);
				$this->kelas_model->tambahkelas($datakelas);
				$this->session->set_flashdata('success', 'Kelas Baru Berhasil Ditambahkan');
			}
			$data['keterangan'] = 'List Kelas';
            $this->load->model('kelas_model');     
            $data['userRecords'] = $this->kelas_model->listkelas();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Tambah Kelas';
            $this->loadViews("kelas/tambahkelas", $this->global, $data, NULL);
        }
    }
	
	public function editkelas($id_kelas = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			if($id_kelas == null)
            {
                redirect('kelas');
            }
			$row = $this->kelas_model->detailkelasbyid($id_kelas);
			$data = array(
			'id_kelas' => $row->id_kelas,
			'nama_kelas' => $row->nama_kelas,
			'jurusan' => $row->jurusan
			);
			
			$data['keterangan'] = 'List Kelas';
            $this->load->model('kelas_model');     
            $data['userRecords'] = $this->kelas_model->listkelas();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Tambah Kelas';
            $this->loadViews("kelas/editkelas", $this->global, $data, NULL);
        }
    }
		
    public function editkelasnya()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$this->load->library('form_validation');
            
			$this->form_validation->set_rules('jurusan', 'jurusan', 'trim|required|xss_clean');			
			$this->form_validation->set_rules('id_kelas', 'id kelas', 'trim|required|cekkelas|xss_clean');
			$this->form_validation->set_rules('kelas', 'kelas', 'trim|required|cekkelas|xss_clean');            
            if($this->form_validation->run() == FALSE)
            {
                $this->tambahkelas();
            }
            else
            {
				$id_kelas = $this->input->post('id_kelas');
				$jurusan = $this->input->post('jurusan');
				$kelas = $this->input->post('kelas');
				$datakelas = array('jurusan'=>$jurusan,'nama_kelas'=> $kelas);
				$this->kelas_model->perbaruikelas($id_kelas, $datakelas);
				$this->session->set_flashdata('success', 'Edit Kelas Berhasil Dilakukan');
			}
			$data['keterangan'] = 'List Kelas';
            $this->load->model('kelas_model');     
            $data['userRecords'] = $this->kelas_model->listkelas();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Tambah Kelas';
            $this->loadViews("kelas/tambahkelas", $this->global, $data, NULL);
        }
    }
	
	function cekkelas()
    {
        $kelas = $this->input->post("kelas");
        $jurusan = $this->input->post("jurusan");
        $result = $this->kelas_model->cekkelas($kelas, $jurusan);
        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
	
	function cekjurusan()
    {
        $kelas = $this->input->post("kelas");
        $jurusan = $this->input->post("jurusan");
        $result = $this->kelas_model->cekkelas($kelas, $jurusan);
        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
	
	function hapuskelas()
    {
        if($this->isAdmin() == TRUE)
        {
            echo "<script type=\"text/javascript\">alert('Anda Tidak Memiliki Hak Untuk Melakukan Aksi Ini');</script>";
        }
        else
        {
            $id_kelas = $this->input->post('id_kelas');
            $result1 = $this->kelas_model->hapuskelas($id_kelas);
        }
    }
}
?>