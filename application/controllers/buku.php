<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Buku extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('buku_model');
		$this->load->model('user_model');
		$this->load->model('kategori_buku_model');
		$this->load->model('peminjaman_model');
		$this->load->model('pengembalian_model');
		$this->load->model('kehilangan_model');
        $this->isLoggedIn();   
    }
    
    public function index()
    {
		$data['keterangan'] = 'List Buku';
        $this->load->model('buku_model');     
        $data['bukuRecords'] = $this->buku_model->listbuku();
        $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Buku';
        $this->loadViews("buku/listbuku", $this->global, $data, NULL);
    }
	
	public function kehilanganbuku()
    {
		$data['keterangan'] = 'List Kehilangan Buku';
        $this->load->model('kehilangan_model');     
        $data['bukuRecords'] = $this->kehilangan_model->listkehilanganbuku();
        $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Kehilangan Buku';
        $this->loadViews("buku/listkehilangan", $this->global, $data, NULL);
    }
	
	function deletekehilanganBuku()
    {
        if($this->isAdmin() == TRUE)
        {
            echo "<script type=\"text/javascript\">alert('Anda Tidak Memiliki Hak Untuk Melakukan Tindakan Ini');</script>";
        }
        else
        {
            $id_kehilangan_buku = $this->input->post('id_kehilangan_buku');
            $result1 = $this->kehilangan_model->delete($id_kehilangan_buku);
        }
    }
	
	public function Detailbuku($NIB)
    {
        $data = $this->buku_model->getDetailBuku($NIB);
        echo json_encode($data);
    }
	
	public function tambahbuku()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'Buku';
            $this->load->model('kategori_buku_model');     
            $data['kategori'] = $this->kategori_buku_model->listkategoribuku();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Tambah Buku';
            $this->loadViews("buku/tambahbuku", $this->global, $data, NULL);
        }
    }
	
	function tambahbukubaru()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('NIB', 'NIB', 'trim|required|xss_clean');           
			$this->form_validation->set_rules('kategori', 'kategori', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jenis_buku', 'jenis buku', 'trim|required|xss_clean');
			$this->form_validation->set_rules('judul_buku', 'judul buku', 'trim|required|xss_clean');
			$this->form_validation->set_rules('penerbit', 'penerbit', 'trim|required|xss_clean');
			$this->form_validation->set_rules('pengarang', 'pengarang', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|xss_clean');
			$this->form_validation->set_rules('ISBN', 'isbn', 'trim|required|xss_clean');
			$this->form_validation->set_rules('edisi', 'edisi', 'trim|required|xss_clean');
			$this->form_validation->set_rules('bahasa', 'bahasa', 'trim|required|xss_clean');
			$this->form_validation->set_rules('asal', 'asal', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tgl_masuk', 'tgl masuk', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tahun_terbit', 'tahun terbit', 'trim|required|xss_clean');
			$this->form_validation->set_rules('deskripsi_buku', 'deskripsi buku', 'trim|required|xss_clean');
			$this->form_validation->set_rules('lokasi_buku', 'lokasi buku', 'trim|required|xss_clean');

            if($this->form_validation->run() == FALSE)
            {
                $this->tambahbuku();
            }
            else
            {
                $data = array(
						'NIB' => $this->input->post('NIB',TRUE),
						'id_kategori' => $this->input->post('kategori',TRUE),
						'jenis_buku' => $this->input->post('jenis_buku',TRUE),
						'judul_buku' => $this->input->post('judul_buku',TRUE),
						'penerbit' => $this->input->post('penerbit',TRUE),
						'pengarang' => $this->input->post('pengarang',TRUE),
						'jumlah' => $this->input->post('jumlah',TRUE),
						'ISBN' => $this->input->post('ISBN',TRUE),
						'edisi' => $this->input->post('edisi',TRUE),
						'bahasa' => $this->input->post('bahasa',TRUE),
						'asal' => $this->input->post('asal',TRUE),
						'tgl_masuk' => $this->input->post('tgl_masuk',TRUE),
						'tahun_terbit' => $this->input->post('tahun_terbit',TRUE),
						'deskripsi_buku' => $this->input->post('deskripsi_buku',TRUE),
						'lokasi_buku' => $this->input->post('lokasi_buku',TRUE),
						);
            $this->buku_model->tambahbuku($data);
            $this->session->set_flashdata('success', 'Berhasil Menambahkan Buku Baru');
			redirect(site_url('buku/tambahbuku'));
            }
        }
    }
	
	public function tambahkehilanganbuku()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'Kehilangan Buku';
            $this->load->model('kategori_buku_model');     
            $data['kategori'] = $this->kategori_buku_model->listkategoribuku();
            $data['buku'] = $this->buku_model->listbuku();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Tambah Buku';
            $this->loadViews("buku/tambahkehilanganbuku", $this->global, $data, NULL);
        }
    }
	
	function tambahkehilanganbukubaru()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('NIB', 'NIB', 'trim|required|xss_clean');           
			$this->form_validation->set_rules('tgl_hilang', 'tahun terbit', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jumlah_hilang', 'deskripsi buku', 'trim|required|xss_clean');
			$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|xss_clean');

            if($this->form_validation->run() == FALSE)
            {
                $this->tambahkehilanganbuku();
            }
            else
            {
                $data = array(
						'id_kehilangan_buku' => date('YmdHis'),
						'id_petugas' => $this->no_identitas,
						'NIB' => $this->input->post('NIB',TRUE),
						'tgl_hilang' => $this->input->post('tgl_hilang',TRUE),
						'jumlah_hilang' => $this->input->post('jumlah_hilang',TRUE),
						'keterangan' => $this->input->post('keterangan',TRUE),
						);
            $this->kehilangan_model->tambahkehilanganbuku($data);
            $this->session->set_flashdata('success', 'Berhasil Menambahkan Buku Baru');
			redirect(site_url('buku/tambahkehilanganbuku'));
            }
        }
    }
	
	public function Editkehilanganbuku($id)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('buku_model');			
            $this->load->model('kehilangan_model');
            $row = $this->kehilangan_model->get_by_id($id);
			$data = array(
			'id_kehilangan_buku' => $row->id_kehilangan_buku,
			'jumlah_hilang' => $row->jumlah_hilang,
			'tgl_hilang' => $row->tgl_hilang,
			'keterangan_buku' => $row->keterangan,
			'NIB' => $row->NIB
			);
            $data['kategori'] = $this->kategori_buku_model->listkategoribuku();
			$data['buku'] = $this->buku_model->listbuku();
			$data['keterangan'] = 'Kehilangan Buku';
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Edit Kehilangan Buku';
            $this->loadViews("buku/editkehilanganbuku", $this->global, $data, NULL);
        }
    }
	
	function Editkehilanganbukunya()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('id', 'id', 'trim|required|xss_clean'); 
			$this->form_validation->set_rules('NIB', 'NIB', 'trim|required|xss_clean');           
			$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|xss_clean');
			$this->form_validation->set_rules('tgl_hilang', 'tanggal hilang', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jumlah_hilang', 'jumlah hilang', 'trim|required|xss_clean');

            if($this->form_validation->run() == FALSE)
            {
                $this->Editkehilanganbuku($this->input->post('id'));
            }
            else
            {
				$id = $this->input->post('id');
                $data = array(
						'NIB' => $this->input->post('NIB',TRUE),
						'keterangan' => $this->input->post('keterangan',TRUE),
						'tgl_hilang' => $this->input->post('tgl_hilang',TRUE),
						'jumlah_hilang' => $this->input->post('jumlah_hilang',TRUE),
						'id_petugas' => $this->no_identitas,
						);
            $this->kehilangan_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Menambahkan Kehilangan Buku Baru');
			redirect(site_url('buku/kehilanganbuku'));
            }
        }
    }
	
	public function Editbuku($NIB)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('buku_model');			
            $this->load->model('kategori_buku_model');
            $row = $this->buku_model->getDetailBuku($NIB);
			$data = array(
			'NIB' => $row->NIB,
			'id_kategori' => $row->id_kategori,
			'jenis_buku' => $row->jenis_buku,
			'judul_buku' => $row->judul_buku,
			'penerbit' => $row->penerbit,
			'pengarang' => $row->pengarang,
			'jumlah' => $row->jumlah,
			'ISBN' => $row->ISBN,
			'edisi' => $row->edisi,
			'bahasa' => $row->bahasa,
			'asal' => $row->asal,
			'tgl_masuk' => $row->tgl_masuk,
			'tahun_terbit' => $row->tahun_terbit,
			'deskripsi_buku' => $row->deskripsi_buku,
			'lokasi_buku' => $row->lokasi_buku,
			);
            $data['kategori'] = $this->kategori_buku_model->listkategoribuku();
			$data['keterangan'] = 'Buku';
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Edit Buku';
            $this->loadViews("buku/editbuku", $this->global, $data, NULL);
        }
    }
	
	function Editbukunya()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('id', 'id', 'trim|required|xss_clean'); 
			$this->form_validation->set_rules('NIB', 'NIB', 'trim|required|xss_clean');           
			$this->form_validation->set_rules('kategori', 'kategori', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jenis_buku', 'jenis buku', 'trim|required|xss_clean');
			$this->form_validation->set_rules('judul_buku', 'judul buku', 'trim|required|xss_clean');
			$this->form_validation->set_rules('penerbit', 'penerbit', 'trim|required|xss_clean');
			$this->form_validation->set_rules('pengarang', 'pengarang', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|xss_clean');
			$this->form_validation->set_rules('ISBN', 'isbn', 'trim|required|xss_clean');
			$this->form_validation->set_rules('edisi', 'edisi', 'trim|required|xss_clean');
			$this->form_validation->set_rules('bahasa', 'bahasa', 'trim|required|xss_clean');
			$this->form_validation->set_rules('asal', 'asal', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tgl_masuk', 'tgl masuk', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tahun_terbit', 'tahun terbit', 'trim|required|xss_clean');
			$this->form_validation->set_rules('deskripsi_buku', 'deskripsi buku', 'trim|required|xss_clean');
			$this->form_validation->set_rules('lokasi_buku', 'lokasi buku', 'trim|required|xss_clean');

            if($this->form_validation->run() == FALSE)
            {
                $this->Editbuku($this->input->post('NIB'));
            }
            else
            {
				$id = $this->input->post('id');
                $data = array(
						'NIB' => $this->input->post('NIB',TRUE),
						'id_kategori' => $this->input->post('kategori',TRUE),
						'jenis_buku' => $this->input->post('jenis_buku',TRUE),
						'judul_buku' => $this->input->post('judul_buku',TRUE),
						'penerbit' => $this->input->post('penerbit',TRUE),
						'pengarang' => $this->input->post('pengarang',TRUE),
						'jumlah' => $this->input->post('jumlah',TRUE),
						'ISBN' => $this->input->post('ISBN',TRUE),
						'edisi' => $this->input->post('edisi',TRUE),
						'bahasa' => $this->input->post('bahasa',TRUE),
						'asal' => $this->input->post('asal',TRUE),
						'tgl_masuk' => $this->input->post('tgl_masuk',TRUE),
						'tahun_terbit' => $this->input->post('tahun_terbit',TRUE),
						'deskripsi_buku' => $this->input->post('deskripsi_buku',TRUE),
						'lokasi_buku' => $this->input->post('lokasi_buku',TRUE),
						);
            $this->buku_model->editbuku($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Menambahkan Buku Baru');
			redirect(site_url('buku'));
            }
        }
    }
	
	function cekNIB()
    {
        $NIB = $this->input->post("NIB");
		$ISBN = $this->input->post("ISBN");
		if(empty($ISBN)){
            $result = $this->buku_model->cekNIB($NIB);
        } else {
            $result = $this->buku_model->cekNIB($NIB, $ISBN);
        }
        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
	
	function cekISBN()
    {
        $ISBN = $this->input->post("ISBN");
		$NIB = $this->input->post("NIB");
		if(empty($NIB)){
            $result = $this->buku_model->cekISBN($ISBN);
        } else {
            $result = $this->buku_model->cekISBN($ISBN, $NIB);
        }
        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
		
	function deleteBuku()
    {
        if($this->isAdmin() == TRUE)
        {
            echo "<script type=\"text/javascript\">alert('Anda Tidak Memiliki Hak Untuk Melakukan Tindakan Ini');</script>";
        }
        else
        {
            $NIB = $this->input->post('NIB');
            $result1 = $this->buku_model->hapusbuku($NIB);
        }
    }

	function deletepengembalianbuku()
    {
        if($this->isAdmin() == TRUE)
        {
            echo "<script type=\"text/javascript\">alert('Anda Tidak Memiliki Hak Untuk Melakukan Tindakan Ini');</script>";
        }
        else
        {
            $id_pengembalian = $this->input->post('id_pengembalian');
            $result1 = $this->pengembalian_model->delete($id_pengembalian);
        }
    }
	
	function deletepeminjamanbuku()
    {
        if($this->isAdmin() == TRUE)
        {
            echo "<script type=\"text/javascript\">alert('Anda Tidak Memiliki Hak Untuk Melakukan Tindakan Ini');</script>";
        }
        else
        {
            $id_peminjaman = $this->input->post('id_peminjaman');
            $result1 = $this->peminjaman_model->delete($id_peminjaman);
        }
    }
	
	public function kategoribuku()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'Kategori Buku';
            $this->load->model('buku_model');     
            $data['userRecords'] = $this->kategori_buku_model->listkategoribuku();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Buku';
            $this->loadViews("buku/listkategoribuku", $this->global, $data, NULL);
        }
    }
	
    public function tambahkategoribukubaru()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$this->load->library('form_validation');
            
			$this->form_validation->set_rules('kategori', 'kategori', 'trim|required|xss_clean');           
            if($this->form_validation->run() == FALSE)
            {
                $this->tambahbuku();
            }
            else
            {
				$kategori = $this->input->post('kategori');
				$databuku = array('nama_kategori'=>$kategori);
				$this->kategori_buku_model->tambahkategoribuku($databuku);
				$this->session->set_flashdata('success', 'Buku Baru Berhasil Ditambahkan');
			}
			$data['keterangan'] = 'Kategori Buku';
            $this->load->model('buku_model');     
            $data['userRecords'] = $this->kategori_buku_model->listkategoribuku();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Tambah Buku';
            $this->loadViews("buku/listkategoribuku", $this->global, $data, NULL);
        }
    }
	
	function hapuskategoribuku()
    {
        if($this->isAdmin() == TRUE)
        {
            echo "<script type=\"text/javascript\">alert('Anda Tidak Memiliki Hak Untuk Melakukan Tindakan Ini');</script>";
        }
        else
        {
            $id_kategori = $this->input->post('id_kategori');
            $result1 = $this->kategori_buku_model->hapuskategoribuku($id_kategori);
        }
    }
	
	public function editkategoribuku($id_kategori = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			if($id_kategori == null)
            {
                redirect('buku/kategoribuku');
            }
			$row = $this->kategori_buku_model->detailkategoribukubyid($id_kategori);
			$data = array(
			'id_kategori' => $row->id_kategori,
			'nama_kategori' => $row->nama_kategori
			);
			
			$data['keterangan'] = 'List Kategori Buku';
            $this->load->model('kategori_buku_model');     
            $data['userRecords'] = $this->kategori_buku_model->listkategoribuku();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Tambah Buku';
            $this->loadViews("buku/editkategoribuku", $this->global, $data, NULL);
        }
    }
	
    public function editkategoribukunya()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$this->load->library('form_validation');
            
			$this->form_validation->set_rules('id_kategori', 'id kategori', 'trim|required|xss_clean');			
			$this->form_validation->set_rules('kategori', 'nama kategori', 'trim|required|xss_clean');       
            if($this->form_validation->run() == FALSE)
            {
                $this->editkategoribuku();
            }
            else
            {
				$id_kategori = $this->input->post('id_kategori');
				$nama_kategori = $this->input->post('kategori');
				$datakategoribuku = array('nama_kategori'=>$nama_kategori);
				$this->kategori_buku_model->perbaruikategoribuku($id_kategori, $datakategoribuku);
				$this->session->set_flashdata('success', 'Edit Buku Berhasil Dilakukan');
			}
			$data['keterangan'] = 'List Kategori Buku';
            $this->load->model('kategori_buku_model');     
            $data['userRecords'] = $this->kategori_buku_model->listkategoribuku();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Tambah Kategori Buku';
            $this->loadViews("buku/listkategoribuku", $this->global, $data, NULL);
        }
    }
	
	function cekkategori()
    {
        $kategori = $this->input->post("kategori");
        $result = $this->kategori_buku_model->cekkategori($kategori);
        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
	
	public function peminjamansiswa()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'Peminjaman Buku Siswa';
            $this->load->model('peminjaman_model');     
            $data['peminjaman'] = $this->peminjaman_model->get_all_siswa();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Peminjaman Buku Siswa';
            $this->loadViews("buku/listpeminjamansiswa", $this->global, $data, NULL);
        }
    }
	
	public function pinjambukusiswa($id)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'Peminjaman Buku Siswa';
            $this->load->model('peminjaman_model');     
			
			$data['detailbuku'] = $this->buku_model->getDetailBuku($id);
            $data['peminjaman'] = $this->peminjaman_model->get_all_siswabyid($id);
			$data['jumlahpeminjamansiswa'] = $this->peminjaman_model->get_jumlah_by_id_buku_siswa($id);
			$data['jumlahpeminjamangurustaff'] = $this->peminjaman_model->get_jumlah_by_id_buku_guru_staff($id);
			$data['jumlahpeminjamanpetugas'] = $this->peminjaman_model->get_jumlah_by_id_buku_petugas($id);
			$data['siswa'] = $this->user_model->userSiswa();
			
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Peminjaman Buku Siswa';
            $this->loadViews("buku/pinjambukusiswa", $this->global, $data, NULL);
        }
    }
	
	function tambahpinjambukusiswa()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('id', 'id', 'trim|required|xss_clean');           
			$this->form_validation->set_rules('no_identitas', 'no_identitas', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tgl_pinjam', 'tgl_pinjam', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tgl_harus_kembali', 'tgl_harus_kembali', 'trim|required|xss_clean');
            if($this->form_validation->run() == FALSE)
            {
                $this->pinjambukusiswa($this->input->post('id'));
            }
            else
            {
				$username=$this->user_model->getDetailInfoSiswa($this->input->post('no_identitas',TRUE))->username;
                $data = array(
						'NIB' => $this->input->post('id',TRUE),
						'id_petugas' => $this->no_identitas,
						'NIS' => $this->input->post('no_identitas',TRUE),
						'total_buku' => $this->input->post('jumlah',TRUE),
						'tgl_pinjam' => $this->input->post('tgl_pinjam',TRUE),
						'tgl_harus_kembali' => $this->input->post('tgl_harus_kembali',TRUE),
						'sudah_kembali_atau_belum' => 't',
						'id_peminjaman' => date('YmdHis'),
						'id_transaksi' => date('ymd').$username,
						);
            $this->peminjaman_model->tambahpinjambuku($data);
            $this->session->set_flashdata('success', 'Berhasil Menambahkan Buku Baru');
			redirect(site_url('buku/peminjamansiswa'));
            }
        }
    }
	
	public function peminjamangurustaff()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'Peminjaman Buku Guru / Staff';
            $this->load->model('peminjaman_model');     
            $data['peminjaman'] = $this->peminjaman_model->get_all_gurustaff();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Peminjaman Buku Guru / Staff';
            $this->loadViews("buku/listpeminjamangurustaff", $this->global, $data, NULL);
        }
    }
	
	public function pinjambukugurustaff($id)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'Peminjaman Buku Siswa';
            $this->load->model('peminjaman_model');     
			
			$data['detailbuku'] = $this->buku_model->getDetailBuku($id);
            $data['peminjaman'] = $this->peminjaman_model->get_all_gurustaffbyid($id);
			$data['jumlahpeminjamansiswa'] = $this->peminjaman_model->get_jumlah_by_id_buku_siswa($id);
			$data['jumlahpeminjamangurustaff'] = $this->peminjaman_model->get_jumlah_by_id_buku_guru_staff($id);
			$data['jumlahpeminjamanpetugas'] = $this->peminjaman_model->get_jumlah_by_id_buku_petugas($id);
			$data['guru'] = $this->user_model->userGuruStaff();
			
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Peminjaman Buku Guru / Staff';
            $this->loadViews("buku/pinjambukugurustaff", $this->global, $data, NULL);
        }
    }
	
	function tambahpinjambukugurustaff()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('id', 'id', 'trim|required|xss_clean');           
			$this->form_validation->set_rules('no_identitas', 'no_identitas', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tgl_pinjam', 'tgl_pinjam', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tgl_harus_kembali', 'tgl_harus_kembali', 'trim|required|xss_clean');
            if($this->form_validation->run() == FALSE)
            {
                $this->pinjambukugurustaff($this->input->post('id'));
            }
            else
            {
				$username=$this->user_model->getDetailInfoGuruStaff($this->input->post('no_identitas',TRUE))->username;
                $data = array(
						'NIP' => $this->input->post('no_identitas',TRUE),
						'id_petugas' => $this->no_identitas,
						'NIB' => $this->input->post('id',TRUE),
						'total_buku' => $this->input->post('jumlah',TRUE),
						'tgl_pinjam' => $this->input->post('tgl_pinjam',TRUE),
						'tgl_harus_kembali' => $this->input->post('tgl_harus_kembali',TRUE),
						'sudah_kembali_atau_belum' => 't',
						'id_peminjaman' => date('YmdHis'),
						'id_transaksi' => date('ymd').$username,
						);
            $this->peminjaman_model->tambahpinjambuku($data);
            $this->session->set_flashdata('success', 'Berhasil Menambahkan Buku Baru');
			redirect(site_url('buku/peminjamangurustaff'));
            }
        }
    }
	/*
	public function peminjamanpetugas()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'Peminjaman Buku Petugas';
            $this->load->model('peminjaman_model');     
            $data['peminjaman'] = $this->peminjaman_model->get_all_petugas();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Peminjaman Buku Petugas';
            $this->loadViews("buku/listpeminjamanpetugas", $this->global, $data, NULL);
        }
    }
	
	public function pinjambukupetugas($id)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'Peminjaman Buku Petugas';
            $this->load->model('peminjaman_model');     
			
			$data['detailbuku'] = $this->buku_model->getDetailBuku($id);
            $data['peminjaman'] = $this->peminjaman_model->get_all_petugasbyid($id);
			$data['jumlahpeminjamansiswa'] = $this->peminjaman_model->get_jumlah_by_id_buku_siswa($id);
			$data['jumlahpeminjamangurustaff'] = $this->peminjaman_model->get_jumlah_by_id_buku_guru_staff($id);
			$data['jumlahpeminjamanpetugas'] = $this->peminjaman_model->get_jumlah_by_id_buku_petugas($id);
			$data['petugas'] = $this->user_model->userPetugas();
			
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Peminjaman Buku Petugas';
            $this->loadViews("buku/pinjambukupetugas", $this->global, $data, NULL);
        }
    }
	
	function tambahpinjambukupetugas()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('id', 'id', 'trim|required|xss_clean');           
			$this->form_validation->set_rules('no_identitas', 'no_identitas', 'trim|required|xss_clean');
			$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tgl_pinjam', 'tgl_pinjam', 'trim|required|xss_clean');
			$this->form_validation->set_rules('tgl_harus_kembali', 'tgl_harus_kembali', 'trim|required|xss_clean');
            if($this->form_validation->run() == FALSE)
            {
                $this->pinjambukupetugas($this->input->post('id'));
            }
            else
            {
                $data = array(
						'NIB' => $this->input->post('id',TRUE),
						'id_petugas' => $this->input->post('no_identitas',TRUE),
						'total_buku' => $this->input->post('jumlah',TRUE),
						'tgl_pinjam' => $this->input->post('tgl_pinjam',TRUE),
						'tgl_harus_kembali' => $this->input->post('tgl_harus_kembali',TRUE),
						'sudah_kembali_atau_belum' => 't',
						'id_peminjaman' => date('YmdHis'),
						);
            $this->peminjaman_model->tambahpinjambuku($data);
            $this->session->set_flashdata('success', 'Berhasil Menambahkan Buku Baru');
			redirect(site_url('buku/peminjamanpetugas'));
            }
        }
    }
	*/
	
	public function pengembalianbuku($id, $no_identitas=null)
    {
        if(($this->isAdmin() == TRUE)|| ($no_identitas==null))
        {
            $this->loadThis();
        }
        else
        {	
            $this->load->model('peminjaman_model');     
			$data['usertype'] = $this->peminjaman_model->get_usertype($no_identitas);
			
			if ($data['usertype']->usertype == "1"){
				$data['siswa'] = $this->user_model->userPetugas();
				$data['peminjaman'] = $this->pengembalian_model->get_all_petugasbyid($id);
				$data['predikat'] = "Petugas ( ".$data['peminjaman']->jabatan." )";
			} else if ($data['usertype']->usertype == "2"){
				$data['siswa'] = $this->user_model->userGuruStaff();
				$data['peminjaman'] = $this->pengembalian_model->get_all_gurustaffbyid($id);
				$data['predikat'] = "Guru / Staff ( ".$data['peminjaman']->status." )";
				$data['keterangan'] = 'Pengembalian Buku'.$data['peminjaman']->status;
			} else if ($data['usertype']->usertype == "3"){
				$data['siswa'] = $this->user_model->userSiswa();
				$data['peminjaman'] = $this->pengembalian_model->get_all_siswabyid($id);
				$data['predikat'] = "Siswa Kelas ".$data['peminjaman']->nama_kelas;
				$data['keterangan'] = 'Pengembalian Buku Siswa';
			}
			$id_buku=$data['peminjaman']->NIB;
			$id_peminjaman=$data['peminjaman']->id_peminjaman;
			$tgl_harus_kembali=date_create($data['peminjaman']->tgl_harus_kembali);
			$tgl_sekarang=date_create(date('Y-m-d'));
			$selisih  = date_diff( $tgl_harus_kembali, $tgl_sekarang );
			$data['keteranganketerlambatan'] = $selisih->days;

			$data['detailbuku'] = $this->buku_model->getDetailBuku($id_buku);
			$data['jumlahpeminjamansiswa'] = $this->peminjaman_model->get_jumlah_by_id_buku_siswa($id_buku);
			$data['jumlahpeminjamangurustaff'] = $this->peminjaman_model->get_jumlah_by_id_buku_guru_staff($id_buku);
			$data['jumlahpeminjamanpetugas'] = $this->peminjaman_model->get_jumlah_by_id_buku_petugas($id_buku);
			$data['pengembalian'] = $this->pengembalian_model->get_by_id($id);
			$data['jumlahdikembalikan'] = $this->pengembalian_model->get_jumlah_by_id($id_peminjaman);
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Peminjaman Buku';
            $this->loadViews("buku/pengembalianbuku", $this->global, $data, NULL);
        }
    }
	
	public function editpengembalianbuku($id)
    {
        if(($this->isAdmin() == TRUE))
        {
            $this->loadThis();
        }
        else
        {	
			$this->load->model('pengembalian_model');
			$row = $this->pengembalian_model->get_by_id_kembali($id);
			$data = array(
			'total_buku_dikembalikan' => $row->total_buku_dikembalikan,
			'denda_keterlambatan' => $row->denda_keterlambatan,
			'denda_kerusakan_buku' => $row->denda_kerusakan_buku,
			'denda_kehilangan_buku' => $row->denda_kehilangan_buku,
			'jumlah_denda_dibayarkan' => $row->jumlah_denda_dibayarkan,
			'tgl_buku_dikembalikan' => $row->tgl_buku_dikembalikan,
			'keterangan_buku' => $row->keterangan,
			'idnya' => $row->id_pengembalian
			);
			$id_pinjam=$this->pengembalian_model->get_by_id_kembali($id)->id_peminjaman;
            $this->load->model('peminjaman_model');
			$no_id=$this->peminjaman_model->get_all_petugasbyid($id_pinjam);
			$no_identitas=$no_id->NIP.$no_id->NIS;
			$data['usertype'] = $this->peminjaman_model->get_usertype($no_identitas);
			
			if ($data['usertype']->usertype == "1"){
				$data['siswa'] = $this->user_model->userPetugas();
				$data['peminjaman'] = $this->pengembalian_model->get_all_petugasbyid($id_pinjam);
				$data['predikat'] = "Petugas ( ".$data['peminjaman']->jabatan." )";
			} else if ($data['usertype']->usertype == "2"){
				$data['siswa'] = $this->user_model->userGuruStaff();
				$data['peminjaman'] = $this->pengembalian_model->get_all_gurustaffbyid($id_pinjam);
				$data['predikat'] = "Guru / Staff ( ".$data['peminjaman']->status." )";
				$data['keterangan'] = 'Pengembalian Buku'.$data['peminjaman']->status;
			} else if ($data['usertype']->usertype == "3"){
				$data['siswa'] = $this->user_model->userSiswa();
				$data['peminjaman'] = $this->pengembalian_model->get_all_siswabyid($id_pinjam);
				$data['predikat'] = "Siswa Kelas ".$data['peminjaman']->nama_kelas;
				$data['keterangan'] = 'Pengembalian Buku Siswa';
			}
			$id_buku=$data['peminjaman']->NIB;
			$id_peminjaman=$data['peminjaman']->id_peminjaman;
			$tgl_harus_kembali=date_create($data['peminjaman']->tgl_harus_kembali);
			$tgl_sekarang=date_create(date('Y-m-d'));
			$selisih  = date_diff( $tgl_harus_kembali, $tgl_sekarang );
			$data['keteranganketerlambatan'] = $selisih->days;

			$data['detailbuku'] = $this->buku_model->getDetailBuku($id_buku);
			$data['jumlahpeminjamansiswa'] = $this->peminjaman_model->get_jumlah_by_id_buku_siswa($id_buku);
			$data['jumlahpeminjamangurustaff'] = $this->peminjaman_model->get_jumlah_by_id_buku_guru_staff($id_buku);
			$data['jumlahpeminjamanpetugas'] = $this->peminjaman_model->get_jumlah_by_id_buku_petugas($id_buku);
			$data['pengembalian'] = $this->pengembalian_model->get_by_id($id);
			$data['jumlahdikembalikan'] = $this->pengembalian_model->get_jumlah_by_id($id_peminjaman);
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Edit Peminjaman Buku';
            $this->loadViews("buku/editpengembalianbuku", $this->global, $data, NULL);
        }
    }
	
	function editpengembalianbukunya()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|xss_clean');
			$this->form_validation->set_rules('jumlah_denda_dibayarkan', 'jumlah_denda_dibayarkan', 'trim|xss_clean');
			$this->form_validation->set_rules('id', 'id', 'trim|required|xss_clean');           
            if($this->form_validation->run() == FALSE )
            {
				echo validation_errors();
                //redirect(site_url('buku'));
            }
            else
            {
				$total=$this->input->post('total',TRUE);
				
				$id_kembali=$this->input->post('id',TRUE);
				$totalpeminjam=$this->peminjaman_model->get_jumlah($id_kembali)->total_buku;
				if ($total=="Semua"){
					$total=$totalpeminjam;
					$updatedata = array(
						'sudah_kembali_atau_belum' => 'y',
						);
					$this->peminjaman_model->update($id_kembali, $updatedata);
				} else { $total=$total; }
				if (empty($this->input->post('denda_kehilangan_buku',TRUE))){$denda_kehilangan_buku=0;} else {$denda_kehilangan_buku=$this->input->post('denda_kehilangan_buku',TRUE);}
				if (empty($this->input->post('denda_kerusakan_buku',TRUE))){$denda_kerusakan_buku=0;} else {$denda_kerusakan_buku=$this->input->post('denda_kerusakan_buku',TRUE);}
				if (empty($this->input->post('jumlah_denda_dibayarkan',TRUE))){$jumlah_denda_dibayarkan=0;} else {$jumlah_denda_dibayarkan=$this->input->post('jumlah_denda_dibayarkan',TRUE);}
				if (empty($this->input->post('keterangan',TRUE))){$keterangan="-";} else {$keterangan=$this->input->post('keterangan',TRUE);}
                $data = array(
						'jumlah_denda_dibayarkan' => $jumlah_denda_dibayarkan,
						'keterangan' => $keterangan,
						);
            $this->pengembalian_model->update($id_kembali, $data);
			
            $this->session->set_flashdata('success', 'Berhasil Menambahkan Pengembalian Baru');
			redirect(site_url('buku/historipengembalian'));
            }
        }
    }
	
	function tambahpengembalianbuku()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('total', 'total', 'trim|required|xss_clean');
			$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|xss_clean');
			$this->form_validation->set_rules('denda_kerusakan_buku', 'denda_kerusakan_buku', 'trim|xss_clean');
			$this->form_validation->set_rules('denda_kehilangan_buku', 'denda_kehilangan_buku', 'trim|xss_clean');
			$this->form_validation->set_rules('jumlah_denda_dibayarkan', 'jumlah_denda_dibayarkan', 'trim|xss_clean');
			$this->form_validation->set_rules('tgl_buku_dikembalikan', 'tgl_buku_dikembalikan', 'trim|required|xss_clean');
			$this->form_validation->set_rules('denda', 'denda', 'trim|required|xss_clean');
			$this->form_validation->set_rules('id', 'id', 'trim|required|xss_clean');           
			$this->form_validation->set_rules('id_buku', 'id_buku', 'trim|required|xss_clean');
            if($this->form_validation->run() == FALSE )
            {
				echo validation_errors();
                //redirect(site_url('buku'));
            }
            else
            {
				$total=$this->input->post('total',TRUE);
				
				$id_kembali=$this->input->post('id',TRUE);
				$totalpeminjam=$this->peminjaman_model->get_jumlah($id_kembali)->total_buku;
				if ($total=="Semua"){
					$total=$totalpeminjam;
					$updatedata = array(
						'sudah_kembali_atau_belum' => 'y',
						);
					$this->peminjaman_model->update($id_kembali, $updatedata);
				} else { $total=$total; }
				if (empty($this->input->post('denda_kehilangan_buku',TRUE))){$denda_kehilangan_buku=0;} else {$denda_kehilangan_buku=$this->input->post('denda_kehilangan_buku',TRUE);}
				if (empty($this->input->post('denda_kerusakan_buku',TRUE))){$denda_kerusakan_buku=0;} else {$denda_kerusakan_buku=$this->input->post('denda_kerusakan_buku',TRUE);}
				if (empty($this->input->post('jumlah_denda_dibayarkan',TRUE))){$jumlah_denda_dibayarkan=0;} else {$jumlah_denda_dibayarkan=$this->input->post('jumlah_denda_dibayarkan',TRUE);}
				if (empty($this->input->post('keterangan',TRUE))){$keterangan="-";} else {$keterangan=$this->input->post('keterangan',TRUE);}
                $data = array(
						'id_peminjaman' => $this->input->post('id',TRUE),
						'denda_keterlambatan' => $this->input->post('denda',TRUE),
						'denda_kehilangan_buku' => $denda_kehilangan_buku,
						'denda_kerusakan_buku' => $denda_kerusakan_buku,
						'jumlah_denda_dibayarkan' => $jumlah_denda_dibayarkan,
						'keterangan' => $keterangan,
						'total_buku_dikembalikan' => $total,
						'tgl_buku_dikembalikan' => $this->input->post('tgl_buku_dikembalikan',TRUE),
						'NIB' => $this->input->post('id_buku',TRUE),
						'id_pengembalian' => date('YmdHis'),
						);
            $this->pengembalian_model->tambahpengembalianbuku($data);
			
            $this->session->set_flashdata('success', 'Berhasil Menambahkan Pengembalian Baru');
			redirect(site_url('buku/historipengembalian'));
            }
        }
    }
	
	public function historipengembalian()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$data['keterangan'] = 'Histori Pengembalian Buku';
            $this->load->model('pengembalian_model');     
            $data['pengembalian'] = $this->pengembalian_model->get_all();
            $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Buku';
            $this->loadViews("buku/listpengembalian", $this->global, $data, NULL);
        }
    }
	
	public function InformasiBuku($id)
    {
        $data['keterangan'] = 'Informasi Buku';
        $this->load->model('peminjaman_model');     
		$data['detailbuku'] = $this->buku_model->getDetailBuku($id);	
		$data['jumlahpeminjamansiswa'] = $this->peminjaman_model->get_jumlah_by_id_buku_siswa($id);
		$data['jumlahpeminjamangurustaff'] = $this->peminjaman_model->get_jumlah_by_id_buku_guru_staff($id);
		$data['jumlahpeminjamanpetugas'] = $this->peminjaman_model->get_jumlah_by_id_buku_petugas($id);
        $this->global['pageTitle'] = 'Sistem Informasi Perpustakaan SMK YP Gajah Mada Palembang : Informasi Buku';
        $this->loadViews("buku/informasibuku", $this->global, $data, NULL);
    }
}
?>