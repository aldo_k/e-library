<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Peminjaman_model extends CI_Model
{

    public $table = 'tbl_peminjaman';
    public $table2 = 'tbl_kehilangan_buku';
    public $id = 'id_peminjaman';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

	function get_jumlah($id_peminjaman)
    {
		$this->db->select('total_buku');
		$this->db->where('id_peminjaman', $id_peminjaman);
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->row();
    }
	
	function get_all_siswa()
    {
        $this->db->select('peminjaman.id_peminjaman, peminjaman.id_transaksi, peminjaman.NIS, peminjaman.NIB, peminjaman.total_buku, peminjaman.tgl_pinjam, peminjaman.tgl_harus_kembali, peminjaman.sudah_kembali_atau_belum, siswa.nama, kelas.nama_kelas, buku.judul_buku, buku.ISBN, user.usertype');
        $this->db->from('tbl_peminjaman as peminjaman');
		$this->db->join('tbl_buku as buku', 'buku.NIB = peminjaman.NIB','inner');
        $this->db->join('tbl_anggota as siswa', 'siswa.NIS = peminjaman.NIS','inner');
		$this->db->join('tbl_pengguna as user', 'siswa.NIS = user.no_identitas','inner');
		$this->db->join('tbl_kelas as kelas', 'siswa.id_kelas = kelas.id_kelas','inner');
		$this->db->where('usertype', '3');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_all_gurustaff()
    {
        $this->db->select('peminjaman.id_peminjaman, peminjaman.id_transaksi,peminjaman.NIP, peminjaman.NIB, peminjaman.total_buku, peminjaman.tgl_pinjam, peminjaman.tgl_harus_kembali, peminjaman.sudah_kembali_atau_belum, gurustaff.nama, gurustaff.jabatan, gurustaff.status, buku.judul_buku, buku.ISBN, user.usertype');
        $this->db->from('tbl_peminjaman as peminjaman');
		$this->db->join('tbl_buku as buku', 'buku.NIB = peminjaman.NIB','inner');
        $this->db->join('tbl_guru_dan_staff as gurustaff', 'gurustaff.NIP = peminjaman.NIP','inner');
		$this->db->join('tbl_pengguna as user', 'gurustaff.NIP = user.no_identitas','inner');
		$this->db->where('usertype', '2');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_all_petugas()
    {
        $this->db->select('peminjaman.id_peminjaman, peminjaman.id_transaksi, peminjaman.id_petugas, peminjaman.NIB, peminjaman.total_buku, peminjaman.tgl_pinjam, peminjaman.tgl_harus_kembali, peminjaman.sudah_kembali_atau_belum, petugas.nama, petugas.jabatan, buku.judul_buku, buku.ISBN, user.usertype');
        $this->db->from('tbl_peminjaman as peminjaman');
		$this->db->join('tbl_buku as buku', 'buku.NIB = peminjaman.NIB','inner');
        $this->db->join('tbl_petugas as petugas', 'petugas.id_petugas = peminjaman.id_petugas','inner');
		$this->db->join('tbl_pengguna as user', 'petugas.id_petugas = user.no_identitas','inner');
		$this->db->where('usertype', '1');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_all_siswabyid($id)
    {
        $this->db->select('peminjaman.id_peminjaman, peminjaman.id_transaksi, peminjaman.NIS, peminjaman.NIB, peminjaman.total_buku, peminjaman.tgl_pinjam, peminjaman.tgl_harus_kembali, siswa.nama, kelas.nama_kelas, buku.judul_buku, buku.ISBN, user.usertype, peminjaman.sudah_kembali_atau_belum');
        $this->db->from('tbl_peminjaman as peminjaman');
		$this->db->join('tbl_buku as buku', 'buku.NIB = peminjaman.NIB','inner');
        $this->db->join('tbl_anggota as siswa', 'siswa.NIS = peminjaman.NIS','inner');
		$this->db->join('tbl_pengguna as user', 'siswa.NIS = user.no_identitas','inner');
		$this->db->join('tbl_kelas as kelas', 'siswa.id_kelas = kelas.id_kelas','inner');
		$this->db->where('peminjaman.NIB', $id);
		$this->db->where('usertype', '3');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_all_gurustaffbyid($id)
    {
        $this->db->select('peminjaman.id_peminjaman, peminjaman.id_transaksi, peminjaman.NIP, peminjaman.NIB, peminjaman.total_buku, peminjaman.tgl_pinjam, peminjaman.tgl_harus_kembali, gurustaff.nama, gurustaff.status, gurustaff.jabatan, buku.judul_buku, buku.ISBN, user.usertype, peminjaman.sudah_kembali_atau_belum');
        $this->db->from('tbl_peminjaman as peminjaman');
		$this->db->join('tbl_buku as buku', 'buku.NIB = peminjaman.NIB','inner');
        $this->db->join('tbl_guru_dan_staff as gurustaff', 'gurustaff.NIP = peminjaman.NIP','inner');
		$this->db->join('tbl_pengguna as user', 'gurustaff.NIP = user.no_identitas','inner');
		$this->db->where('peminjaman.NIB', $id);
		$this->db->where('usertype', '2');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_all_petugasbyid($id)
    {
        $this->db->select('peminjaman.id_peminjaman, peminjaman.id_transaksi, peminjaman.id_petugas, peminjaman.NIP, peminjaman.NIS, peminjaman.NIB, peminjaman.total_buku, peminjaman.tgl_pinjam, peminjaman.tgl_harus_kembali, petugas.nama, petugas.jabatan, buku.judul_buku, buku.ISBN, user.usertype, peminjaman.sudah_kembali_atau_belum');
        $this->db->from('tbl_peminjaman as peminjaman');
		$this->db->join('tbl_buku as buku', 'buku.NIB = peminjaman.NIB','inner');
        $this->db->join('tbl_petugas as petugas', 'petugas.id_petugas = peminjaman.id_petugas','inner');
		$this->db->join('tbl_pengguna as user', 'petugas.id_petugas = user.no_identitas','inner');
		$this->db->where('peminjaman.id_peminjaman', $id);
        $query = $this->db->get();
        return $query->row();
    }
	
	function get_jumlah_by_id_buku_siswa($id_buku)
    {
		$this->db->select_sum('total_buku');
		$this->db->where('NIB', $id_buku);
		$this->db->where('sudah_kembali_atau_belum' , 't');
		$this->db->where('NIS !=' , '');
		return $this->db->get($this->table)->row();
	}
	
	function get_jumlah_by_id_buku_guru_staff($id_buku)
    {
		$this->db->select_sum('total_buku');
		$this->db->where('NIB', $id_buku);
		$this->db->where('sudah_kembali_atau_belum' , 't');
		$this->db->where('NIP !=' , '');
		return $this->db->get($this->table)->row();
	}
	
	function get_jumlah_by_id_buku_petugas($id_buku)
    {
		$this->db->select_sum('jumlah_hilang');
		$this->db->where('NIB', $id_buku);
		return $this->db->get($this->table2)->row();
	}
	
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

	function get_usertype($id)
    {
        $this->db->select('usertype');
        $this->db->from('tbl_pengguna');
		$this->db->where('no_identitas', $id);
        $query = $this->db->get();
        return $query->row();
    }
	
    function tambahpinjambuku($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Tbl_peminjaman_model.php */
/* Location: ./application/models/Tbl_peminjaman_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-06-16 12:32:23 */
/* http://harviacode.com */