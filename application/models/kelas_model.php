<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelas_model extends CI_Model
{

    public $table = 'tbl_kelas';
    public $id = 'id_kelas';
    public $order = 'ASC';

    function __construct()
    {
        parent::__construct();
    }
	
    function listkelas()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function detailkelasbyid($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // insert data
    function tambahkelas($data)
    {
        $this->db->insert($this->table, $data);
    }

	function cekkelas($kelas, $jurusan = null)
    {
        $this->db->select('nama_kelas');
        $this->db->from('tbl_kelas');
        $this->db->where('nama_kelas', $kelas);
		if($jurusan != null){
            $this->db->where("jurusan =", $jurusan);
        }
        $query = $this->db->get();
        return $query->result();
    }
	
	function cekjurusan($kelas, $jurusan = null)
    {
        $this->db->select('nama_kelas');
        $this->db->from('tbl_kelas');
        $this->db->where('nama_kelas', $kelas);
		if($jurusan != null){
            $this->db->where("jurusan =", $jurusan);
        }
        $query = $this->db->get();
        return $query->result();
    }
	
    // update data
    function perbaruikelas($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function hapuskelas($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}