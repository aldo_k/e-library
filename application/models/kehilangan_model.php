<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kehilangan_model extends CI_Model
{

    public $table = 'tbl_kehilangan_buku';
    public $id = 'id_kehilangan_buku';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }
    function listkehilanganbuku()
    {
		$this->db->select('tbl_buku.*, tbl_kehilangan_buku.*, tbl_petugas.*');
		$this->db->from('tbl_buku');
		$this->db->join('tbl_kehilangan_buku', 'tbl_buku.NIB = tbl_kehilangan_buku.NIB','inner');
		$this->db->join('tbl_petugas', 'tbl_petugas.id_petugas = tbl_kehilangan_buku.id_petugas','inner');
        $this->db->order_by($this->id, $this->order);
        $query = $this->db->get();
        return $query->result();
    }

    function get_by_id($id)
    {
        $this->db->where('id_kehilangan_buku', $id);
        return $this->db->get($this->table)->row();
    }

    // insert data
    function tambahkehilanganbuku($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
	
	function get_all_siswabyid($id)
    {
        $this->db->select('peminjaman.id_peminjaman, peminjaman.NIS, peminjaman.NIB, peminjaman.total_buku, peminjaman.tgl_pinjam, peminjaman.tgl_harus_kembali, siswa.nama, kelas.nama_kelas, buku.judul_buku, buku.ISBN, user.usertype, peminjaman.sudah_kembali_atau_belum');
        $this->db->from('tbl_peminjaman as peminjaman');
		$this->db->join('tbl_buku as buku', 'buku.NIB = peminjaman.NIB','inner');
        $this->db->join('tbl_anggota as siswa', 'siswa.NIS = peminjaman.NIS','inner');
		$this->db->join('tbl_pengguna as user', 'siswa.NIS = user.no_identitas','inner');
		$this->db->join('tbl_kelas as kelas', 'siswa.id_kelas = kelas.id_kelas','inner');
		$this->db->where('peminjaman.id_peminjaman', $id);
		$this->db->where('usertype', '3');
        $query = $this->db->get();
        return $query->row();
    }
	
	function get_all_gurustaffbyid($id)
    {
        $this->db->select('peminjaman.id_peminjaman, peminjaman.NIP, peminjaman.NIB, peminjaman.total_buku, peminjaman.tgl_pinjam, peminjaman.tgl_harus_kembali, gurustaff.nama, gurustaff.status, gurustaff.jabatan, buku.judul_buku, buku.ISBN, user.usertype, peminjaman.sudah_kembali_atau_belum');
        $this->db->from('tbl_peminjaman as peminjaman');
		$this->db->join('tbl_buku as buku', 'buku.NIB = peminjaman.NIB','inner');
        $this->db->join('tbl_guru_dan_staff as gurustaff', 'gurustaff.NIP = peminjaman.NIP','inner');
		$this->db->join('tbl_pengguna as user', 'gurustaff.NIP = user.no_identitas','inner');
		$this->db->where('peminjaman.id_peminjaman', $id);
		$this->db->where('usertype', '2');
        $query = $this->db->get();
        return $query->row();
    }
	
	function get_all_petugasbyid($id)
    {
        $this->db->select('peminjaman.id_peminjaman, peminjaman.id_petugas, peminjaman.NIB, peminjaman.total_buku, peminjaman.tgl_pinjam, peminjaman.tgl_harus_kembali, petugas.nama, petugas.jabatan, buku.judul_buku, buku.ISBN, user.usertype, peminjaman.sudah_kembali_atau_belum');
        $this->db->from('tbl_peminjaman as peminjaman');
		$this->db->join('tbl_buku as buku', 'buku.NIB = peminjaman.NIB','inner');
        $this->db->join('tbl_petugas as petugas', 'petugas.id_petugas = peminjaman.id_petugas','inner');
		$this->db->join('tbl_pengguna as user', 'petugas.id_petugas = user.no_identitas','inner');
		$this->db->where('peminjaman.id_peminjaman', $id);
		$this->db->where('usertype', '1');
        $query = $this->db->get();
        return $query->row();
    }
	
	function get_jumlah_by_id($id_peminjaman)
    {
		$this->db->select_sum('total_buku_dikembalikan');
		$this->db->where('id_peminjaman', $id_peminjaman);
		return $this->db->get($this->table)->row();
	}

}