<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
	function userAll()
    {
        $this->db->select('username, password, usertype, nama, no_identitas');
        $this->db->from('tbl_pengguna');
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    
    function userPetugas()
    {
        $this->db->select('pengguna.username, pengguna.usertype, petugas.nama, pengguna.no_identitas, petugas.tempat_lahir, petugas.tgl_lahir, petugas.alamat, petugas.telepon, petugas.jenis_kelamin, petugas.jabatan, petugas.foto, (select sum(total_buku) as jumlah from tbl_peminjaman INNER JOIN tbl_pengguna ON tbl_pengguna.no_identitas=tbl_peminjaman.id_petugas where tbl_peminjaman.id_petugas=tbl_pengguna.no_identitas AND tbl_peminjaman.sudah_kembali_atau_belum='.'"'.'t'.'"'.') as jumlahbuku');
        $this->db->from('tbl_pengguna as pengguna');
        $this->db->join('tbl_petugas as petugas', 'petugas.id_petugas = pengguna.no_identitas','inner');
		$this->db->where('usertype', '1');
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
	
	function userPetugasbyID($id)
    {
        $this->db->select('pengguna.username, pengguna.usertype, petugas.nama, pengguna.no_identitas, petugas.tempat_lahir, petugas.tgl_lahir, petugas.alamat, petugas.telepon, petugas.jenis_kelamin, petugas.jabatan, petugas.foto');
        $this->db->from('tbl_pengguna as pengguna');
        $this->db->join('tbl_petugas as petugas', 'petugas.id_petugas = pengguna.no_identitas','inner');
        $this->db->where('no_identitas', $id);
		$this->db->where('usertype', '1');
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
	
	function userGuruStaff()
    {
        $this->db->select('pengguna.username, pengguna.usertype, gurustaff.nama, pengguna.no_identitas, gurustaff.tempat_lahir, gurustaff.tgl_lahir, gurustaff.alamat, gurustaff.telepon, gurustaff.jenis_kelamin, gurustaff.jabatan, gurustaff.status, gurustaff.foto');
        $this->db->from('tbl_pengguna as pengguna');
        $this->db->join('tbl_guru_dan_staff as gurustaff', 'gurustaff.NIP = pengguna.no_identitas','inner');
		$this->db->where('usertype', '2');
		$this->db->group_by('pengguna.no_identitas'); 
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
	
	function jumlahpinjamGuruStaffbyID($id)
    {
        $this->db->select('sum(tbl_peminjaman.total_buku) as jumlahbuku');
        $this->db->from('tbl_pengguna as pengguna');
        $this->db->join('tbl_guru_dan_staff as gurustaff', 'gurustaff.NIP = pengguna.no_identitas','inner');
		$this->db->join('tbl_peminjaman', 'tbl_peminjaman.NIP = gurustaff.NIP','inner');
        $this->db->where('no_identitas', $id);
		$this->db->where('tbl_peminjaman.sudah_kembali_atau_belum', 't');
		$this->db->where('usertype', '2');
        $query = $this->db->get();
        $result = $query->row();        
        return $result;
    }
	
	function userGuruStaffbyID($id)
    {
        $this->db->select('pengguna.username, pengguna.usertype, gurustaff.nama, pengguna.no_identitas, gurustaff.tempat_lahir, gurustaff.tgl_lahir, gurustaff.alamat, gurustaff.telepon, gurustaff.jenis_kelamin, gurustaff.jabatan, gurustaff.status, gurustaff.foto');
        $this->db->from('tbl_pengguna as pengguna');
        $this->db->join('tbl_guru_dan_staff as gurustaff', 'gurustaff.NIP = pengguna.no_identitas','inner');
        $this->db->where('no_identitas', $id);
		$this->db->where('usertype', '2');
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
	
	function userSiswa()
    {
        $this->db->select('pengguna.username, pengguna.usertype, siswa.nama, pengguna.no_identitas, siswa.tempat_lahir, siswa.tgl_lahir, siswa.alamat, siswa.telepon, siswa.jenis_kelamin, kelas.jurusan, siswa.id_kelas, siswa.NIS, siswa.foto, siswa.masa_berlaku, kelas.nama_kelas as kelas');
        $this->db->from('tbl_pengguna as pengguna');
        $this->db->join('tbl_anggota as siswa', 'siswa.NIS = pengguna.no_identitas','inner');
		$this->db->join('tbl_kelas as kelas', 'kelas.id_kelas = siswa.id_kelas','inner');
		$this->db->where('usertype', '3');
		$this->db->group_by('pengguna.no_identitas'); 
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
	
	function jumlahpinjamSiswabyID($id)
    {
        $this->db->select('sum(tbl_peminjaman.total_buku) as jumlahbuku');
        $this->db->from('tbl_pengguna as pengguna');
        $this->db->join('tbl_anggota as siswa', 'siswa.NIS = pengguna.no_identitas','inner');
		$this->db->join('tbl_kelas as kelas', 'kelas.id_kelas = siswa.id_kelas','inner');
		$this->db->join('tbl_peminjaman', 'tbl_peminjaman.NIS = siswa.NIS','inner');
        $this->db->where('no_identitas', $id);
		$this->db->where('tbl_peminjaman.sudah_kembali_atau_belum', 't');
		$this->db->where('usertype', '3');
        $query = $this->db->get();
        $result = $query->row();        
        return $result;
    }
	
	function userSiswabyID($id)
    {
        $this->db->select('pengguna.username, pengguna.usertype, siswa.nama, pengguna.no_identitas, siswa.tempat_lahir, siswa.tgl_lahir, siswa.alamat, siswa.telepon, siswa.jenis_kelamin, kelas.jurusan, siswa.id_kelas, siswa.foto,siswa.masa_berlaku, kelas.nama_kelas as kelas');
        $this->db->from('tbl_pengguna as pengguna');
        $this->db->join('tbl_anggota as siswa', 'siswa.NIS = pengguna.no_identitas','inner');
		$this->db->join('tbl_kelas as kelas', 'kelas.id_kelas = siswa.id_kelas','inner');
        $this->db->where('no_identitas', $id);
		$this->db->where('usertype', '3');
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    
    function cekUsername($username, $no_identitas = 0)
    {
        $this->db->select('username');
        $this->db->from('tbl_pengguna');
        $this->db->where('username', $username);
		if($no_identitas != 0){
            $this->db->where("no_identitas !=", $no_identitas);
        }
        $query = $this->db->get();
        return $query->result();
    }
    
	function cekNoIdentitas($no_identitas, $username=null)
    {
        $this->db->select('no_identitas');
        $this->db->from('tbl_pengguna');
        $this->db->where('no_identitas', $no_identitas);
		if($username != null){
            $this->db->where("username !=", $username);
        }
        $query = $this->db->get();

        return $query->result();
    }
	
    function TambahPenggunaBaru($akunPenggunaBaru)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_pengguna', $akunPenggunaBaru);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
	function TambahAnggotaSiswaBaru($dataLengkapPengguna)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_anggota', $dataLengkapPengguna);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
	function TambahGuruStaffBaru($dataLengkapPengguna)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_guru_dan_staff', $dataLengkapPengguna);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
	function TambahPetugasBaru($dataLengkapPengguna)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_petugas', $dataLengkapPengguna);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
	
    
    function editPengguna($akunPenggunaEdit, $no_identitas)
    {
        $this->db->where('no_identitas', $no_identitas);
        $this->db->update('tbl_pengguna', $akunPenggunaEdit);
        return TRUE;
    }
	
	function editAnggotaSiswa($dataLengkapPengguna, $no_identitas)
    {
        $this->db->where('NIS', $no_identitas);
        $this->db->update('tbl_anggota', $dataLengkapPengguna);
        return TRUE;
    }
	
	function editGuruStaff($dataLengkapPengguna, $no_identitas)
    {
        $this->db->where('NIP', $no_identitas);
        $this->db->update('tbl_guru_dan_staff', $dataLengkapPengguna);
        return TRUE;
    }
	
	function editPetugas($dataLengkapPengguna, $no_identitas)
    {
        $this->db->where('id_petugas', $no_identitas);
        $this->db->update('tbl_petugas', $dataLengkapPengguna);
        return TRUE;
    }
	
	
	function listkelas($jurusan)
    {
        $this->db->select('id_kelas, nama_kelas'); 
		 $this->db->from('tbl_kelas');
		 $this->db->where('jurusan', $jurusan); 
		 $this->db->order_by('nama_kelas', 'asc');
		 return $this->db->get()->result(); 
    }

    function getDetailInfoPetugas($no_identitas)
    {
        $this->db->select('pengguna.username, pengguna.usertype, petugas.nama, pengguna.no_identitas, petugas.tempat_lahir, petugas.tgl_lahir, petugas.alamat, petugas.telepon, petugas.jenis_kelamin, petugas.jabatan, petugas.foto');
        $this->db->from('tbl_pengguna as pengguna');
        $this->db->join('tbl_petugas as petugas', 'petugas.id_petugas = pengguna.no_identitas','inner');
        $this->db->where('no_identitas', $no_identitas);
		$this->db->where('usertype', '1');
        $query = $this->db->get();
        
        return $query->row();
    }
	
	function getDetailInfoGuruStaff($no_identitas)
    {
        $this->db->select('pengguna.username, pengguna.usertype, gurustaff.nama, pengguna.no_identitas, gurustaff.tempat_lahir, gurustaff.tgl_lahir, gurustaff.alamat, gurustaff.telepon, gurustaff.jenis_kelamin, gurustaff.jabatan, gurustaff.status, gurustaff.foto');
        $this->db->from('tbl_pengguna as pengguna');
        $this->db->join('tbl_guru_dan_staff as gurustaff', 'gurustaff.NIP = pengguna.no_identitas','inner');
        $this->db->where('no_identitas', $no_identitas);
		$this->db->where('usertype', '2');
        $query = $this->db->get();
        
        return $query->row();
    }
	
	function getDetailInfoSiswa($no_identitas)
    {
        $this->db->select('pengguna.username, pengguna.usertype, siswa.nama, pengguna.no_identitas, siswa.tempat_lahir, siswa.tgl_lahir, siswa.alamat, siswa.telepon, siswa.jenis_kelamin, kelas.jurusan, siswa.id_kelas, siswa.foto,siswa.masa_berlaku, kelas.nama_kelas as kelas');
        $this->db->from('tbl_pengguna as pengguna');
        $this->db->join('tbl_anggota as siswa', 'siswa.NIS = pengguna.no_identitas','inner');
		$this->db->join('tbl_kelas as kelas', 'kelas.id_kelas = siswa.id_kelas','inner');
        $this->db->where('no_identitas', $no_identitas);
		$this->db->where('usertype', '3');
        $query = $this->db->get();
        
        return $query->row();
    }
    
    
    function deleteAkun($no_identitas)
    {
        $this->db->where('no_identitas', $no_identitas);
        $this->db->delete('tbl_pengguna');
    }
	
	function deletePetugas($no_identitas)
    {
        $this->db->where('id_petugas', $no_identitas);
        $this->db->delete('tbl_petugas');
    }
	
	function deleteGuruStaff($no_identitas)
    {
        $this->db->where('NIP', $no_identitas);
        $this->db->delete('tbl_guru_dan_staff');
    }
	
	function deleteSiswa($no_identitas)
    {
        $this->db->where('NIS', $no_identitas);
        $this->db->delete('tbl_anggota');
    }

    function matchOldPassword($no_identitas, $oldPassword)
    {
        $this->db->select('no_identitas, password');
        $this->db->where('no_identitas', $no_identitas);        
        $query = $this->db->get('tbl_pengguna');
        
        $user = $query->result();

        if(!empty($user)){
            if(verifyHashedPassword($oldPassword, $user[0]->password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }
 
    function changePassword($no_identitas, $dataLengkapPengguna)
    {
        $this->db->where('no_identitas', $no_identitas);
        $this->db->update('tbl_pengguna', $dataLengkapPengguna);
        
        return $this->db->affected_rows();
    }
}

  