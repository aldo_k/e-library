<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
    
    function loginMe($username, $password)
    {
        $this->db->select('username, password, usertype, no_identitas');
        $this->db->from('tbl_pengguna');
        $this->db->where('username', $username);
        $query = $this->db->get();
        $user = $query->result();
        
        if(!empty($user)){
            if(verifyHashedPassword($password, $user[0]->password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }

    
    function checkusernameExist($username)
    {
        $this->db->select('username');
        $this->db->where('username', $username);
        $query = $this->db->get('tbl_pengguna');

        if ($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }
	
}

?>