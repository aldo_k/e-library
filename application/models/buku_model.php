<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Buku_model extends CI_Model
{

    public $table = 'tbl_buku';
    public $id = 'NIB';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    function listbuku()
    {
		$this->db->select('tbl_buku.*, (select sum(total_buku) as jumlah from tbl_peminjaman INNER JOIN tbl_buku ON tbl_buku.NIB=tbl_peminjaman.NIB where tbl_peminjaman.NIB=tbl_buku.NIB AND tbl_peminjaman.sudah_kembali_atau_belum='.'"'.'t'.'"'.') as jumlahbuku, (select sum(jumlah_hilang) as jumlahhilang from tbl_kehilangan_buku INNER JOIN tbl_buku ON tbl_buku.NIB=tbl_kehilangan_buku.NIB where tbl_kehilangan_buku.NIB=tbl_buku.NIB) as jumlahbukuhilang');
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

	function getDetailBuku($NIB)
    {
        $this->db->select('buku.NIB, buku.jenis_buku, buku.judul_buku, buku.penerbit, buku.pengarang, buku.jumlah, buku.ISBN, buku.edisi, buku.bahasa, buku.asal, buku.tgl_masuk, buku.tahun_terbit, buku.deskripsi_buku, buku.lokasi_buku, kategori.nama_kategori, kategori.id_kategori');
        $this->db->from('tbl_buku as buku');
        $this->db->join('tbl_kategori_buku as kategori', 'buku.id_kategori = kategori.id_kategori','inner');
        $this->db->where('NIB', $NIB);
        $query = $this->db->get();
        
        return $query->row();
    }
	
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // insert data
    function tambahbuku($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function editbuku($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function hapusbuku($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
	
	function cekNIB($NIB, $ISBN=null)
    {
        $this->db->select('NIB');
        $this->db->from('tbl_buku');
        $this->db->where('NIB', $NIB);
		if($ISBN != null){
            $this->db->where("ISBN !=", $ISBN);
        }
        $query = $this->db->get();
        return $query->result();
    }
	
	function cekISBN($ISBN, $NIB=null)
    {
        $this->db->select('ISBN');
        $this->db->from('tbl_buku');
        $this->db->where('ISBN', $ISBN);
		if($NIB != null){
            $this->db->where("NIB !=", $NIB);
        }
        $query = $this->db->get();
        return $query->result();
    }

}