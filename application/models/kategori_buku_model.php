<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kategori_buku_model extends CI_Model
{

    public $table = 'tbl_kategori_buku';
    public $id = 'id_kategori';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function listkategoribuku()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function detailkategoribukubyid($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // insert data
    function tambahkategoribuku($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function perbaruikategoribuku($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function hapuskategoribuku($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
	
	function cekkategori($kategori)
    {
        $this->db->select('nama_kategori');
        $this->db->from('tbl_kategori_buku');
        $this->db->where('nama_kategori', $kategori);
        $query = $this->db->get();
        return $query->result();
    }

}

/* End of file Tbl_kategori_buku_model.php */
/* Location: ./application/models/Tbl_kategori_buku_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-06-16 12:32:23 */
/* http://harviacode.com */