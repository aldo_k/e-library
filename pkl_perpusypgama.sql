-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 25 Jul 2017 pada 12.11
-- Versi Server: 5.7.17-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pkl_perpusypgama`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_anggota`
--

CREATE TABLE `tbl_anggota` (
  `NIS` varchar(18) NOT NULL,
  `nama` varchar(42) NOT NULL,
  `tempat_lahir` varchar(25) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(14) NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `id_kelas` int(4) NOT NULL,
  `foto` text NOT NULL,
  `masa_berlaku` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_anggota`
--

INSERT INTO `tbl_anggota` (`NIS`, `nama`, `tempat_lahir`, `tgl_lahir`, `alamat`, `telepon`, `jenis_kelamin`, `id_kelas`, `foto`, `masa_berlaku`) VALUES
('011010101123', 'Aldo Karendra', 'Pendopo', '2017-07-31', 'Pendopo', '0895343128858', 'L', 1, '01101010112320170717121406.jpg', '2017'),
('0110101011231', 'Aldo Karendra X', 'Pendopo', '2017-07-30', 'Pendopo', '0895343128858', 'L', 1, '011010101123120170716081755.jpg', '2017'),
('01114002818', 'Aldo Karendra', 'Pendopo', '2017-06-30', 'Pendopo, Talang Ubi, PALI', '0895343128858', 'L', 1, '0111400281820170718002744.jpg', '2018');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_buku`
--

CREATE TABLE `tbl_buku` (
  `NIB` varchar(8) NOT NULL,
  `id_kategori` int(2) NOT NULL,
  `jenis_buku` varchar(32) NOT NULL,
  `judul_buku` varchar(128) NOT NULL,
  `penerbit` varchar(64) NOT NULL,
  `pengarang` varchar(64) NOT NULL,
  `jumlah` int(3) NOT NULL,
  `ISBN` varchar(24) NOT NULL,
  `edisi` varchar(12) NOT NULL,
  `bahasa` varchar(24) NOT NULL,
  `asal` varchar(32) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `tahun_terbit` varchar(4) NOT NULL,
  `deskripsi_buku` text NOT NULL,
  `lokasi_buku` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_buku`
--

INSERT INTO `tbl_buku` (`NIB`, `id_kategori`, `jenis_buku`, `judul_buku`, `penerbit`, `pengarang`, `jumlah`, `ISBN`, `edisi`, `bahasa`, `asal`, `tgl_masuk`, `tahun_terbit`, `deskripsi_buku`, `lokasi_buku`) VALUES
('123123', 1, 'Buku ( UMUM )', 'Coba Judul', 'Asal Terbit', 'Siapa Aja', 100, '123123', '12', 'Indonesia', 'Sumbangan', '2017-07-30', '2013', 'asdas dasd as das d as d asd as d a sdasd   as das das', 'dasdsdasdas asd as das d asd a sdas dasdasdasdas d');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_guru_dan_staff`
--

CREATE TABLE `tbl_guru_dan_staff` (
  `NIP` varchar(18) NOT NULL,
  `nama` varchar(42) NOT NULL,
  `tempat_lahir` varchar(25) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(14) NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `status` varchar(5) NOT NULL,
  `jabatan` varchar(23) NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_guru_dan_staff`
--

INSERT INTO `tbl_guru_dan_staff` (`NIP`, `nama`, `tempat_lahir`, `tgl_lahir`, `alamat`, `telepon`, `jenis_kelamin`, `status`, `jabatan`, `foto`) VALUES
('011010101', 'Uji Coba', 'Pendopo', '2017-07-12', 'Pendopo', '0895343128858', 'L', 'Guru', 'Bahasa Inggris', '01101010120170715130142.jpg'),
('01101010112', 'Rama', 'Pendopo', '2017-08-01', 'Pendopo', '0895343128858', 'L', 'Guru', 'Bahasa Inggris', '0110101011220170718011327.png'),
('0111400281818', 'Aldo Karendra', 'Pendopo', '2017-07-01', 'Pendopo, Talang Ubi, PALI', '0895343128858', 'L', 'Staff', 'Petugas', '011140028181820170718003130.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kategori_buku`
--

CREATE TABLE `tbl_kategori_buku` (
  `id_kategori` int(2) NOT NULL,
  `nama_kategori` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_kategori_buku`
--

INSERT INTO `tbl_kategori_buku` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Kategori 1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kehilangan_buku`
--

CREATE TABLE `tbl_kehilangan_buku` (
  `id_kehilangan_buku` varchar(14) NOT NULL,
  `id_petugas` varchar(14) NOT NULL,
  `NIB` varchar(8) NOT NULL,
  `tgl_hilang` date NOT NULL,
  `jumlah_hilang` int(3) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kelas`
--

CREATE TABLE `tbl_kelas` (
  `id_kelas` int(2) NOT NULL,
  `nama_kelas` varchar(17) NOT NULL,
  `jurusan` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_kelas`
--

INSERT INTO `tbl_kelas` (`id_kelas`, `nama_kelas`, `jurusan`) VALUES
(1, 'X TKL 1', 'Teknik Instalasi Tenaga Listrik'),
(2, 'X TKL 2', 'Teknik Instalasi Tenaga Listrik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_peminjaman`
--

CREATE TABLE `tbl_peminjaman` (
  `id_peminjaman` varchar(14) NOT NULL,
  `id_transaksi` varchar(18) NOT NULL,
  `NIS` varchar(18) NOT NULL,
  `NIP` varchar(18) NOT NULL,
  `id_petugas` varchar(14) NOT NULL,
  `NIB` varchar(8) NOT NULL,
  `total_buku` int(4) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `tgl_harus_kembali` date NOT NULL,
  `sudah_kembali_atau_belum` enum('t','y') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_peminjaman`
--

INSERT INTO `tbl_peminjaman` (`id_peminjaman`, `id_transaksi`, `NIS`, `NIP`, `id_petugas`, `NIB`, `total_buku`, `tgl_pinjam`, `tgl_harus_kembali`, `sudah_kembali_atau_belum`) VALUES
('20170719235053', '170719aldo_k10', '011010101123', '', '011140028', '123123', 12, '2017-07-12', '2017-07-24', 't'),
('20170719235630', '170719aldo_k10', '011010101123', '', '011140028', '123123', 1, '2017-07-17', '2017-07-21', 'y'),
('20170720003607', '170720aldo_k10', '011010101123', '', '011140028', '123123', 5, '2017-07-23', '2017-08-03', 't');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pengembalian`
--

CREATE TABLE `tbl_pengembalian` (
  `id_pengembalian` varchar(14) NOT NULL,
  `id_peminjaman` varchar(14) NOT NULL,
  `NIB` varchar(8) NOT NULL,
  `total_buku_dikembalikan` int(3) NOT NULL,
  `denda_keterlambatan` varchar(6) NOT NULL,
  `denda_kerusakan_buku` varchar(6) NOT NULL,
  `denda_kehilangan_buku` varchar(6) NOT NULL,
  `jumlah_denda_dibayarkan` varchar(6) NOT NULL,
  `tgl_buku_dikembalikan` date NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_pengembalian`
--

INSERT INTO `tbl_pengembalian` (`id_pengembalian`, `id_peminjaman`, `NIB`, `total_buku_dikembalikan`, `denda_keterlambatan`, `denda_kerusakan_buku`, `denda_kehilangan_buku`, `jumlah_denda_dibayarkan`, `tgl_buku_dikembalikan`, `keterangan`) VALUES
('20170720003525', '20170719235630', '123123', 1, '0', '0', '0', '0', '2017-07-20', '-');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pengguna`
--

CREATE TABLE `tbl_pengguna` (
  `username` varchar(12) NOT NULL,
  `password` varchar(128) NOT NULL,
  `usertype` varchar(1) NOT NULL,
  `no_identitas` varchar(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_pengguna`
--

INSERT INTO `tbl_pengguna` (`username`, `password`, `usertype`, `no_identitas`) VALUES
('aldo1008', '$2y$10$3bHpvMGMtKI7Hb/jduVIoOWu8TwSs5yeymCMxBZWTovzA/tMjNuSm', '3', '0110101011231'),
('aldo_k', '$2y$10$qf/yfVfL5hrllgL7W9OxqOCyGEDY5LkESWr.ueHaxAWBcCg27fZrC', '1', '011140028'),
('aldo_k10', '$2y$10$WYoCP4QVHQ.CTcxNbHXcR.9UiWPZJjyUToYR4LMkw11rkC4tcnUGe', '3', '011010101123'),
('aldo_k100000', '$2y$10$3wuXaIUrrknwZDtGLUyuTe84LGbOVEGM4d2.sFpIe.dNMfU.xY6i.', '2', '0111400281818'),
('aldo_k1008', '$2y$10$GahXlUfjuHQOY9/wxRsdVO3o2.o5QKAL8ctkr4dcdm5sPlAECuGZa', '3', '01114002818'),
('guru_123', '$2y$10$2zZFhKJWbYqEIwVXDwRf3OcxA0Nm1k1MBRxs7KFUQIG7WwckAZUnC', '2', '011010101'),
('rama', '$2y$10$Ga01HxsYdZv4mO9tagRhVefUpNzgDjeGHlqRo0zxZYY/0D1U48vsO', '2', '01101010112');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_petugas`
--

CREATE TABLE `tbl_petugas` (
  `id_petugas` varchar(14) NOT NULL,
  `nama` varchar(42) NOT NULL,
  `tempat_lahir` varchar(25) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(14) NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `jabatan` varchar(23) NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_petugas`
--

INSERT INTO `tbl_petugas` (`id_petugas`, `nama`, `tempat_lahir`, `tgl_lahir`, `alamat`, `telepon`, `jenis_kelamin`, `jabatan`, `foto`) VALUES
('011140028', 'Aldo Karendra', 'Pendopo', '1996-07-09', 'Pendopo, Talang Ubi, PALI', '0895343128858', 'L', 'Petugas', '01114002820170718003150.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_anggota`
--
ALTER TABLE `tbl_anggota`
  ADD PRIMARY KEY (`NIS`);

--
-- Indexes for table `tbl_buku`
--
ALTER TABLE `tbl_buku`
  ADD PRIMARY KEY (`NIB`);

--
-- Indexes for table `tbl_guru_dan_staff`
--
ALTER TABLE `tbl_guru_dan_staff`
  ADD PRIMARY KEY (`NIP`);

--
-- Indexes for table `tbl_kategori_buku`
--
ALTER TABLE `tbl_kategori_buku`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tbl_kehilangan_buku`
--
ALTER TABLE `tbl_kehilangan_buku`
  ADD PRIMARY KEY (`id_kehilangan_buku`);

--
-- Indexes for table `tbl_kelas`
--
ALTER TABLE `tbl_kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `tbl_peminjaman`
--
ALTER TABLE `tbl_peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indexes for table `tbl_pengembalian`
--
ALTER TABLE `tbl_pengembalian`
  ADD PRIMARY KEY (`id_pengembalian`);

--
-- Indexes for table `tbl_pengguna`
--
ALTER TABLE `tbl_pengguna`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `tbl_petugas`
--
ALTER TABLE `tbl_petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_kategori_buku`
--
ALTER TABLE `tbl_kategori_buku`
  MODIFY `id_kategori` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
